#include "appsettings.h"
#include "ui_appsettings.h"
#ifdef __APPLE__
#include "utility.h"
#endif
#include <QMessageBox>

AppSettings::AppSettings(QWidget* parent)
    : QDialog(parent)
    , hotkey_1(new QHotkey(this))
    , ui(new Ui::AppSettings)
{
    ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void AppSettings::setupUI(ScreenCapSettings* settings)
{
    ui->txtImageDimension->setText(QString::number(settings->GetCapDimensions()));
    ui->chkHiddenMode->setChecked(settings->GetHiddenMode());
    ui->chkStartCapturing->setChecked(settings->GetCaptureOnStartup());
    ui->chkStartMinimized->setChecked(settings->GetStartMinimized());
    ui->keySequenceEdit->setKeySequence(settings->GetHotkeySequence());
    ui->HotKeyConfirm->setChecked(settings->GetHotkeyConfirm());
    ui->txtApiUrl->setText(settings->GerApiUrl());
    connect(this->ui->HotKeyConfirm, &QCheckBox::toggled,
        this->hotkey_1, &QHotkey::setRegistered);
    connect(this->ui->keySequenceEdit, &QKeySequenceEdit::keySequenceChanged,
        this, &AppSettings::setShortcut_1);

    this->addItemsToFormatDdl(ui->ddlImgFormat, settings);
    this->addItemsToQualityDdl(ui->ddlQuality, settings);
}

/**
 * @brief AppSettings::setShortcut_1
 * @param sequence
 * To register the Hotkey
 */
void AppSettings::setShortcut_1(const QKeySequence& sequence)
{
    this->hotkey_1->setShortcut(sequence, false);
}
void AppSettings::on_HotKeyConfirm_toggled(bool checked)
{
    if (checked == true) {
        ui->keySequenceEdit->setEnabled(false);

        //  MainWindow::showMinimized();
    }
    else if (checked == false) {
        ui->keySequenceEdit->setEnabled(true);
    }
}

/**
 * @brief AppSettings::GetUpdatedSettings
 * Fetches the updated settings when the user presses the OK button
 * @param settings Current application settings.
 * @return Updated application settings.
 */
ScreenCapSettings* AppSettings::GetUpdatedSettings(ScreenCapSettings* settings)
{
    settings->SetCapDimensions(ui->txtImageDimension->text().toInt());
    settings->SetCaptureOnStartup(ui->chkStartCapturing->isChecked());
    settings->SetHiddenMode(ui->chkHiddenMode->isChecked());
    settings->SetStartMinimized(ui->chkStartMinimized->isChecked());
    settings->setHotkeySequence(ui->keySequenceEdit->keySequence().toString());
    settings->setHotkeyConfirm(ui->HotKeyConfirm->isChecked());
    settings->setApiUrl(ui->txtApiUrl->text());

    int imgQuality = ui->ddlQuality->itemData(ui->ddlQuality->currentIndex()).toInt();
    int imgFormat = ui->ddlImgFormat->itemData(ui->ddlImgFormat->currentIndex()).toInt();
    settings->SetImgFormat(imgFormat);
    settings->SetImgQuality(imgQuality);

    return settings;
}

/**
 * @brief AppSettings::addItemsToQualityDdl
 * Adds items to the quality dropdown and also selects the
 * currently preferred image quality.
 * @param cmbBox Image quality combobox
 * @param settings Application settings
 */
void AppSettings::addItemsToQualityDdl(QComboBox* cmbBox, ScreenCapSettings* settings)
{
    QMap<int, QString> qQualities = settings->GetListOfImgQualities();
    QMap<int, QString>::iterator i;
    int currQuality = settings->GetImgQuality();
    int intIndex = 0;
    int currIndex = -1;
    for (i = qQualities.begin(); i != qQualities.end(); ++i) {
        cmbBox->addItem(i.value(), QVariant(i.key()));
        if (i.key() == currQuality) {
            currIndex = intIndex;
        }
        ++intIndex;
    }
    cmbBox->setCurrentIndex(currIndex);
}

/**
 * @brief AppSettings::addItemsToFormatDdl
 * Adds items to the image format dropdown and also selects the
 * default image
 * @param cmbBox Image format combobox
 * @param settings Application settings.
 */
void AppSettings::addItemsToFormatDdl(QComboBox* cmbBox, ScreenCapSettings* settings)
{
    QMap<int, QString> qFormats = settings->GetListOfImgFormats();
    QMap<int, QString>::iterator i;
    int intIndex = 0;
    int currIndex = -1;
    int currFormat = settings->GetImgFormatInt();
    for (i = qFormats.begin(); i != qFormats.end(); ++i) {
        cmbBox->addItem(i.value(), QVariant(i.key()));
        if (currFormat == i.key()) {
            currIndex = intIndex;
        }
        ++intIndex;
    }
    cmbBox->setCurrentIndex(currIndex);
}

/**
 * @brief AppSettings::done
 * Overriding the base done method to perform validations.
 * If everything is okay, calls the base QDialog::done()
 * to close the dialog.
 * @param result QDialog::result()
 */
void AppSettings::done(int result)
{
    if (QDialog::Accepted == result) {
        if (this->checkSettings()) {
            QDialog::done(result);
        }
        else {

            return;
        }
    }
    else {
        QDialog::done(result);
        return;
    }
}

/**
 * @brief AppSettings::checkSettings
 * Performs validation on the values entered by the user.
 * @return true if everything is okay, else false.
 */
bool AppSettings::checkSettings()
{
    // Validating the dimensions...
    bool isDimensionInt = false;
    QString strDimensions = ui->txtImageDimension->text().trimmed();
    int intDimensions = strDimensions.toInt(&isDimensionInt);
    if (strDimensions.length() != 0 || isDimensionInt) {
        if (intDimensions > MIN_DIMENSION_HEIGHT && intDimensions <= 100) {
            return true;
        }
        this->dimensionError();
        return false;
    }
    this->dimensionError();
    return false;
}

/**
 * @brief AppSettings::dimensionError
 * Clears the textbox and displays a message box
 * when the user enters an incorrect dimension.
 */
void AppSettings::dimensionError()
{
    ui->txtImageDimension->setFocus();
    ui->txtImageDimension->setText("");
    QMessageBox msgBox;
    QFont font(DLG_FONT_FAMILY);
    font.setStyleHint(QFont::TypeWriter);
    msgBox.setWindowTitle("Settings Error");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText("The dimensions should be between " + QString::number(MIN_DIMENSION_HEIGHT) + " and 100.");
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setFont(font);
    msgBox.exec();
}

AppSettings::~AppSettings()
{
    delete ui;
}
/**
 * @brief AppSettings::on_chkStartCapturing_toggled
 * @param checked
 * To add or remove Scrinter as a run on start program in Windows, Linux and Mac Osx.
 */
void AppSettings::on_chkStartCapturing_toggled(bool checked)
{
#ifdef Q_OS_MAC
    QString bundlePath = Utility::GetMacApplicationPath() + "/scrinter.app";
    QFileInfo fileInfo(bundlePath);
#endif
    if (checked == true) {
#ifdef Q_OS_WIN32
        QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
        settings.setValue(APPLICATION_NAME, QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
        settings.sync();
#endif
#ifdef linux
        // Path to the autorun folder
        QString autostartPath = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + QLatin1String("/autostart");
        /* Check whether there is a directory in which to store the autorun file.
             * And then you never know ... user deleted ...
             * */
        QDir autorunDir(autostartPath);
        if (!autorunDir.exists()) {
            autorunDir.mkpath(autostartPath);
        }
        QFile autorunFile(autostartPath + QLatin1String("/Scrinter.desktop"));
        /* Check the state of the checkbox, if checked, adds the application to the startup.
             * Otherwise remove
             * */
        if (ui->chkStartCapturing->isChecked()) {
            // Next, check the availability of the startup file
            if (!autorunFile.exists()) {

                /* Next, open the file and writes the necessary data
                     * with the path to the executable file, using QCoreApplication::applicationFilePath()
                     * */
                if (autorunFile.open(QFile::WriteOnly)) {

                    QString autorunContent("[Desktop Entry]\n"
                                           "Type=Application\n"
                                           "Exec="
                        + QCoreApplication::applicationFilePath() + "\n"
                                                                    "Hidden=false\n"
                                                                    "NoDisplay=false\n"
                                                                    "X-GNOME-Autostart-enabled=true\n"
                                                                    "Name[en_GB]=Scrinter\n"
                                                                    "Name=Scrinter\n"
                                                                    "Comment[en_GB]=Scrinter\n"
                                                                    "Comment=Scrinter\n");
                    QTextStream outStream(&autorunFile);
                    outStream << autorunContent;
                    // Set access rights, including on the performance of the file, otherwise the autorun does not work
                    autorunFile.setPermissions(QFileDevice::ExeUser | QFileDevice::ExeOwner | QFileDevice::ExeOther | QFileDevice::ExeGroup | QFileDevice::WriteUser | QFileDevice::ReadUser);
                    autorunFile.close();
                }
            }
        }
        else {
            // Delete the startup file
            if (autorunFile.exists())
                autorunFile.remove();
        }
#endif
#if defined(Q_OS_MAC)

        // Remove any existing login entry for this app first, in case there was one
        // from a previous installation, that may be under a different launch path.
        {
            QStringList args;
            args << "-e tell application \"System Events\" to delete login item\""
                    + fileInfo.baseName() + "\"";

            QProcess::execute("osascript", args);
        }

        // Now install the login item, if needed.

        QStringList args;
        args << "-e tell application \"System Events\" to make login item at end with properties {path:\"" + bundlePath + "\", hidden:false}";

        QProcess::execute("osascript", args);

#endif
    }
    else if (checked == false) {
#ifdef Q_OS_WIN32
        QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
        settings.remove(APPLICATION_NAME);
#endif
#ifdef linux
        QString autostartPath = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + QLatin1String("/autostart");
        QFile autorunFile(autostartPath + QLatin1String("/Scrinter.desktop"));
        if (autorunFile.exists())
            autorunFile.remove();
#endif
#ifdef Q_OS_MAC

        QStringList args;
        args << "-e tell application \"System Events\" to delete login item\""
                + fileInfo.baseName() + "\"";

        QProcess::execute("osascript", args);
#endif
    }
}
