/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbout;
    QAction *actionHelp;
    QAction *actionSettings;
    QAction *actionAbout_2;
    QAction *actionSettings_2;
    QAction *btnAbout_2;
    QAction *actionHelp_2;
    QAction *actionExit;
    QAction *actionclose;
    QWidget *cntrlMainWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_13;
    QLabel *lblDestination;
    QLineEdit *txtDestinationPath;
    QPushButton *btnDestinationBrowser;
    QHBoxLayout *horizontalLayout;
    QLabel *lblCaptureTime;
    QLineEdit *txtCaptureTime;
    QLabel *lblTmpSeconds;
    QCheckBox *chkRandomCapture;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnStartCapture;
    QPushButton *btnStopCapture;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuTools;
    QMenu *menuHelp;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(552, 120);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(552, 120));
        MainWindow->setMaximumSize(QSize(552, 120));
        QFont font;
        font.setFamily(QStringLiteral("Monospace"));
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/img/img/Desktop.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setWindowOpacity(1);
        MainWindow->setDockNestingEnabled(false);
        MainWindow->setUnifiedTitleAndToolBarOnMac(true);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        actionHelp = new QAction(MainWindow);
        actionHelp->setObjectName(QStringLiteral("actionHelp"));
        actionSettings = new QAction(MainWindow);
        actionSettings->setObjectName(QStringLiteral("actionSettings"));
        actionAbout_2 = new QAction(MainWindow);
        actionAbout_2->setObjectName(QStringLiteral("actionAbout_2"));
        actionSettings_2 = new QAction(MainWindow);
        actionSettings_2->setObjectName(QStringLiteral("actionSettings_2"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/img/img/Gear.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSettings_2->setIcon(icon1);
        btnAbout_2 = new QAction(MainWindow);
        btnAbout_2->setObjectName(QStringLiteral("btnAbout_2"));
        btnAbout_2->setIcon(icon);
        actionHelp_2 = new QAction(MainWindow);
        actionHelp_2->setObjectName(QStringLiteral("actionHelp_2"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/img/img/Get Info Blue Button.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionHelp_2->setIcon(icon2);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/img/img/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon3);
        actionclose = new QAction(MainWindow);
        actionclose->setObjectName(QStringLiteral("actionclose"));
        actionclose->setCheckable(false);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/img/img/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionclose->setIcon(icon4);
        actionclose->setIconVisibleInMenu(true);
        cntrlMainWidget = new QWidget(MainWindow);
        cntrlMainWidget->setObjectName(QStringLiteral("cntrlMainWidget"));
        cntrlMainWidget->setMinimumSize(QSize(540, 0));
        layoutWidget = new QWidget(cntrlMainWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 551, 94));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetNoConstraint);
        verticalLayout->setContentsMargins(7, 7, 7, 7);
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        horizontalLayout_13->setSizeConstraint(QLayout::SetFixedSize);
        horizontalLayout_13->setContentsMargins(10, -1, 10, -1);
        lblDestination = new QLabel(layoutWidget);
        lblDestination->setObjectName(QStringLiteral("lblDestination"));
        sizePolicy.setHeightForWidth(lblDestination->sizePolicy().hasHeightForWidth());
        lblDestination->setSizePolicy(sizePolicy);
        lblDestination->setMaximumSize(QSize(68, 16777215));

        horizontalLayout_13->addWidget(lblDestination);

        txtDestinationPath = new QLineEdit(layoutWidget);
        txtDestinationPath->setObjectName(QStringLiteral("txtDestinationPath"));
        sizePolicy.setHeightForWidth(txtDestinationPath->sizePolicy().hasHeightForWidth());
        txtDestinationPath->setSizePolicy(sizePolicy);
        txtDestinationPath->setMinimumSize(QSize(342, 0));
        txtDestinationPath->setMaximumSize(QSize(342, 25));

        horizontalLayout_13->addWidget(txtDestinationPath);

        btnDestinationBrowser = new QPushButton(layoutWidget);
        btnDestinationBrowser->setObjectName(QStringLiteral("btnDestinationBrowser"));
        btnDestinationBrowser->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(43);
        sizePolicy1.setVerticalStretch(66);
        sizePolicy1.setHeightForWidth(btnDestinationBrowser->sizePolicy().hasHeightForWidth());
        btnDestinationBrowser->setSizePolicy(sizePolicy1);
        btnDestinationBrowser->setMinimumSize(QSize(24, 33));
        btnDestinationBrowser->setBaseSize(QSize(20, 20));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/img/img/Spotlight Blue Button1.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnDestinationBrowser->setIcon(icon5);
        btnDestinationBrowser->setIconSize(QSize(24, 24));

        horizontalLayout_13->addWidget(btnDestinationBrowser);


        verticalLayout->addLayout(horizontalLayout_13);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(6, -1, 8, -1);
        lblCaptureTime = new QLabel(layoutWidget);
        lblCaptureTime->setObjectName(QStringLiteral("lblCaptureTime"));
        sizePolicy.setHeightForWidth(lblCaptureTime->sizePolicy().hasHeightForWidth());
        lblCaptureTime->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(lblCaptureTime);

        txtCaptureTime = new QLineEdit(layoutWidget);
        txtCaptureTime->setObjectName(QStringLiteral("txtCaptureTime"));
        sizePolicy.setHeightForWidth(txtCaptureTime->sizePolicy().hasHeightForWidth());
        txtCaptureTime->setSizePolicy(sizePolicy);
        txtCaptureTime->setMinimumSize(QSize(23, 20));
        txtCaptureTime->setMaximumSize(QSize(20, 20));
        txtCaptureTime->setAcceptDrops(true);
        txtCaptureTime->setInputMethodHints(Qt::ImhDigitsOnly);

        horizontalLayout->addWidget(txtCaptureTime);

        lblTmpSeconds = new QLabel(layoutWidget);
        lblTmpSeconds->setObjectName(QStringLiteral("lblTmpSeconds"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Maximum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lblTmpSeconds->sizePolicy().hasHeightForWidth());
        lblTmpSeconds->setSizePolicy(sizePolicy2);
        lblTmpSeconds->setMinimumSize(QSize(0, 24));

        horizontalLayout->addWidget(lblTmpSeconds);

        chkRandomCapture = new QCheckBox(layoutWidget);
        chkRandomCapture->setObjectName(QStringLiteral("chkRandomCapture"));
        sizePolicy.setHeightForWidth(chkRandomCapture->sizePolicy().hasHeightForWidth());
        chkRandomCapture->setSizePolicy(sizePolicy);
        chkRandomCapture->setMinimumSize(QSize(83, 0));
        chkRandomCapture->setMaximumSize(QSize(20, 25));
        chkRandomCapture->setFont(font);
        chkRandomCapture->setAcceptDrops(false);
        chkRandomCapture->setLayoutDirection(Qt::LeftToRight);

        horizontalLayout->addWidget(chkRandomCapture);

        horizontalSpacer = new QSpacerItem(42, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnStartCapture = new QPushButton(layoutWidget);
        btnStartCapture->setObjectName(QStringLiteral("btnStartCapture"));
        QSizePolicy sizePolicy3(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btnStartCapture->sizePolicy().hasHeightForWidth());
        btnStartCapture->setSizePolicy(sizePolicy3);
        btnStartCapture->setMinimumSize(QSize(110, 0));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/img/img/Play.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStartCapture->setIcon(icon6);
        btnStartCapture->setIconSize(QSize(24, 24));
        btnStartCapture->setFlat(false);

        horizontalLayout->addWidget(btnStartCapture);

        btnStopCapture = new QPushButton(layoutWidget);
        btnStopCapture->setObjectName(QStringLiteral("btnStopCapture"));
        QSizePolicy sizePolicy4(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(110);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(btnStopCapture->sizePolicy().hasHeightForWidth());
        btnStopCapture->setSizePolicy(sizePolicy4);
        btnStopCapture->setMinimumSize(QSize(110, 0));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/img/img/Stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStopCapture->setIcon(icon7);
        btnStopCapture->setIconSize(QSize(24, 24));

        horizontalLayout->addWidget(btnStopCapture);


        verticalLayout->addLayout(horizontalLayout);

        MainWindow->setCentralWidget(cntrlMainWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 552, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuFile->setFocusPolicy(Qt::TabFocus);
        menuFile->setContextMenuPolicy(Qt::CustomContextMenu);
#ifndef QT_NO_TOOLTIP
        menuFile->setToolTip(QStringLiteral("File"));
#endif // QT_NO_TOOLTIP
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QStringLiteral("menuTools"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        MainWindow->setMenuBar(menuBar);
#ifndef QT_NO_SHORTCUT
        lblDestination->setBuddy(txtDestinationPath);
        lblCaptureTime->setBuddy(txtCaptureTime);
#endif // QT_NO_SHORTCUT

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionclose);
        menuFile->addAction(actionExit);
        menuTools->addAction(actionSettings_2);
        menuHelp->addAction(actionHelp_2);
        menuHelp->addAction(btnAbout_2);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Scrinter", Q_NULLPTR));
        actionAbout->setText(QApplication::translate("MainWindow", "Exit", Q_NULLPTR));
        actionHelp->setText(QApplication::translate("MainWindow", "Help", Q_NULLPTR));
        actionSettings->setText(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
        actionAbout_2->setText(QApplication::translate("MainWindow", "About", Q_NULLPTR));
        actionSettings_2->setText(QApplication::translate("MainWindow", "&Settings", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionSettings_2->setToolTip(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionSettings_2->setShortcut(QApplication::translate("MainWindow", "Alt+S", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        btnAbout_2->setText(QApplication::translate("MainWindow", "&About", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnAbout_2->setToolTip(QApplication::translate("MainWindow", "About", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        btnAbout_2->setShortcut(QApplication::translate("MainWindow", "Alt+A", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionHelp_2->setText(QApplication::translate("MainWindow", "He&lp", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionHelp_2->setToolTip(QApplication::translate("MainWindow", "Help", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionExit->setText(QApplication::translate("MainWindow", "&Exit", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionExit->setToolTip(QApplication::translate("MainWindow", "Exit", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionExit->setShortcut(QApplication::translate("MainWindow", "Alt+E", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionclose->setText(QApplication::translate("MainWindow", "&Close", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionclose->setToolTip(QApplication::translate("MainWindow", "Close", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionclose->setShortcut(QApplication::translate("MainWindow", "Alt+C", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        lblDestination->setText(QApplication::translate("MainWindow", "Folder Path", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txtDestinationPath->setToolTip(QApplication::translate("MainWindow", "Folder to save the screenshots in...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btnDestinationBrowser->setToolTip(QApplication::translate("MainWindow", "Browse...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnDestinationBrowser->setText(QApplication::translate("MainWindow", "Browse", Q_NULLPTR));
        lblCaptureTime->setText(QApplication::translate("MainWindow", "Time Period", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txtCaptureTime->setToolTip(QApplication::translate("MainWindow", "How often to take screenshots?", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        lblTmpSeconds->setText(QApplication::translate("MainWindow", "seconds", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        chkRandomCapture->setToolTip(QApplication::translate("MainWindow", "Take screenshots at random...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        chkRandomCapture->setText(QApplication::translate("MainWindow", "at random", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnStartCapture->setToolTip(QApplication::translate("MainWindow", "Start Capturing...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnStartCapture->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnStopCapture->setToolTip(QApplication::translate("MainWindow", "Stop Capturing...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnStopCapture->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("MainWindow", "&File", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        menuTools->setToolTip(QApplication::translate("MainWindow", "Tools", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        menuTools->setTitle(QApplication::translate("MainWindow", "&Tools", Q_NULLPTR));
        menuHelp->setTitle(QApplication::translate("MainWindow", "&Help", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
