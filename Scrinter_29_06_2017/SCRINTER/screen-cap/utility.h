#ifndef UTILITY_H
#define UTILITY_H
#include <QString>
#include <QDir>
#include <QDateTime>
#include <QPixmap>
#include <QSize>
#include <QScreen>
#include <QApplication>
#include <QTextStream>
#include "constants.h"
#include <QSysInfo>
#include <QProcess>
#include <QString>
#include <QHostInfo>
#include <QHostAddress>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkInterface>
#include <QTcpSocket>
#include <QUrl>
#include <QUrlQuery>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>
#ifdef WIN32
    #include <Windows.h>
#endif
class Utility
{
public:
    static bool CheckAndCreateFolder(QString folder, QString path);
    static QString GetDateTimeInString(QString strFormat = FILE_LOG_DATE_FORMAT);
    static int GetRandomBetween(int max, int min);
    static bool WriteLog(QString filePath, QString message, int line = 0, QString srcfileName = "Unknown");
    static QString GetCpuConfig();
    static QString GetLocalIp();
    static QString GetOperatingSystem();
    static QString GetPublicIp();
    static QString GetRamSize();
    static QString GetCurrentUsername();
    static QString GetMacAddress();
    static void PostLogInJson();
    static void PostLogOutJson();
};

#endif // UTILITY_H
