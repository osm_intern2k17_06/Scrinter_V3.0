/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../screen-cap/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[19];
    char stringdata0[427];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 26), // "on_btnStartCapture_clicked"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 25), // "on_btnStopCapture_clicked"
QT_MOC_LITERAL(4, 65, 20), // "timeToTakeScreenshot"
QT_MOC_LITERAL(5, 86, 16), // "resetRandomTimer"
QT_MOC_LITERAL(6, 103, 32), // "on_btnDestinationBrowser_clicked"
QT_MOC_LITERAL(7, 136, 13), // "iconActivated"
QT_MOC_LITERAL(8, 150, 33), // "QSystemTrayIcon::ActivationRe..."
QT_MOC_LITERAL(9, 184, 6), // "reason"
QT_MOC_LITERAL(10, 191, 18), // "on_applicationQuit"
QT_MOC_LITERAL(11, 210, 15), // "hotkeyActivated"
QT_MOC_LITERAL(12, 226, 23), // "on_btnAbout_2_triggered"
QT_MOC_LITERAL(13, 250, 29), // "on_actionSettings_2_triggered"
QT_MOC_LITERAL(14, 280, 23), // "on_actionExit_triggered"
QT_MOC_LITERAL(15, 304, 24), // "on_actionclose_triggered"
QT_MOC_LITERAL(16, 329, 25), // "on_actionHelp_2_triggered"
QT_MOC_LITERAL(17, 355, 37), // "on_txtDestinationPath_editing..."
QT_MOC_LITERAL(18, 393, 33) // "on_txtCaptureTime_editingFini..."

    },
    "MainWindow\0on_btnStartCapture_clicked\0"
    "\0on_btnStopCapture_clicked\0"
    "timeToTakeScreenshot\0resetRandomTimer\0"
    "on_btnDestinationBrowser_clicked\0"
    "iconActivated\0QSystemTrayIcon::ActivationReason\0"
    "reason\0on_applicationQuit\0hotkeyActivated\0"
    "on_btnAbout_2_triggered\0"
    "on_actionSettings_2_triggered\0"
    "on_actionExit_triggered\0"
    "on_actionclose_triggered\0"
    "on_actionHelp_2_triggered\0"
    "on_txtDestinationPath_editingFinished\0"
    "on_txtCaptureTime_editingFinished"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   89,    2, 0x08 /* Private */,
       3,    0,   90,    2, 0x08 /* Private */,
       4,    0,   91,    2, 0x08 /* Private */,
       5,    0,   92,    2, 0x08 /* Private */,
       6,    0,   93,    2, 0x08 /* Private */,
       7,    1,   94,    2, 0x08 /* Private */,
      10,    0,   97,    2, 0x08 /* Private */,
      11,    0,   98,    2, 0x08 /* Private */,
      12,    0,   99,    2, 0x08 /* Private */,
      13,    0,  100,    2, 0x08 /* Private */,
      14,    0,  101,    2, 0x08 /* Private */,
      15,    0,  102,    2, 0x08 /* Private */,
      16,    0,  103,    2, 0x08 /* Private */,
      17,    0,  104,    2, 0x08 /* Private */,
      18,    0,  105,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btnStartCapture_clicked(); break;
        case 1: _t->on_btnStopCapture_clicked(); break;
        case 2: _t->timeToTakeScreenshot(); break;
        case 3: _t->resetRandomTimer(); break;
        case 4: _t->on_btnDestinationBrowser_clicked(); break;
        case 5: _t->iconActivated((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 6: _t->on_applicationQuit(); break;
        case 7: _t->hotkeyActivated(); break;
        case 8: _t->on_btnAbout_2_triggered(); break;
        case 9: _t->on_actionSettings_2_triggered(); break;
        case 10: _t->on_actionExit_triggered(); break;
        case 11: _t->on_actionclose_triggered(); break;
        case 12: _t->on_actionHelp_2_triggered(); break;
        case 13: _t->on_txtDestinationPath_editingFinished(); break;
        case 14: _t->on_txtCaptureTime_editingFinished(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
