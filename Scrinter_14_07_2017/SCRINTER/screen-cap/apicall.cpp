#include "apicall.h"

QString apicall::GetDateTimeInString(QString strFormat) {
    QDateTime now = QDateTime::currentDateTime();
    return now.toString(strFormat);
}

QString apicall::GetCpuConfig(){
    return QSysInfo::currentCpuArchitecture();
}

QString apicall::GetLocalIp(){

    QString localIPAddr;
    QTcpSocket socket;
    socket.connectToHost("8.8.8.8", 53);
    if (socket.waitForConnected()){
        localIPAddr = socket.localAddress().toString();
    }
    return localIPAddr;
}
QString apicall::GetOperatingSystem(){
    return QSysInfo::prettyProductName();
}

QString apicall::GetPublicIp(){
    QEventLoop eventLoop;
    QString publicIPAddr;
    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    // the HTTP request
    QNetworkRequest req( QUrl( QString("http://bot.whatismyipaddress.com/") ) );
    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec(); // blocks stack until "finished()" has been called

    if (reply->error() == QNetworkReply::NoError) {
        //success
        publicIPAddr =reply->readAll();
        delete reply;
    }
    else {
        //failure
        publicIPAddr =reply->errorString();
        delete reply;
    }
    return publicIPAddr;
}

QString apicall::GetRamSize(){
#ifdef _WIN32
    MEMORYSTATUSEX memory_status;
    ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    if (GlobalMemoryStatusEx(&memory_status)) {
        return QString("%1 MB").arg(memory_status.ullTotalPhys / (1024 * 1024));
    } else {
        return "Unknown RAM";
    }

#elif linux
    QProcess p;
    p.start("awk", QStringList() << "/MemTotal/ { print $2 }" << "/proc/meminfo");
    p.waitForFinished();
    QString memory = p.readAllStandardOutput();
    return QString("%1 MB").arg(memory.toLong() / 1024);
    p.close();

#elif __APPLE__
    QProcess p;
    p.start("sysctl", QStringList()<<"hw.physmem");
    p.waitForFinished();
    QString system_info = p.readAllStandardOutput();
    system_info = system_info.remove(0,12);
    system_info = system_info.remove(10,2);
    int ram_size = system_info.toDouble();
    ram_size = (ram_size)/(1024 * 1024);
    system_info = QString::number(ram_size).remove(0,1)+" MB";
    return system_info;
    p.close();

#endif

}
QString apicall:: GetCurrentUsername(){
    return QSysInfo::machineHostName();
}

QString apicall::GetMacAddress()
{
#ifdef __APPLE__
    QString text;
        foreach(QNetworkInterface interface, QNetworkInterface::allInterfaces())
        {
            if(interface.hardwareAddress()!=""){
                text = interface.hardwareAddress();
            }
        }
        return text;
#else
    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
    {
        // Return only the first non-loopback MAC Address
        if (!(netInterface.flags() & QNetworkInterface::IsLoopBack))
            return netInterface.hardwareAddress();
    }
    return QString();
#endif
}
void apicall::PostLogInJson(){
    QJsonObject loginJsonObj;
    loginJsonObj.insert("mac_address",GetMacAddress());
    loginJsonObj.insert("username",GetCurrentUsername());
    loginJsonObj.insert("activity_id",1);
    loginJsonObj.insert("activitytime",GetDateTimeInString("yyyy-MM-dd HH:mm:ss"));
    QJsonDocument loginJsonDoc;
    loginJsonDoc.setObject(loginJsonObj);

    QEventLoop eventLoop;

    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    // the HTTP request
    QNetworkRequest req( QUrl( QString("http://10.0.0.239:8765/users-activities/add") ) );

    req.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/json"));
    mgr.post(req,loginJsonDoc.toJson());
    eventLoop.exec(); // blocks stack until "finished()" has been called// blocks stack until "finished()" has been called

}
void apicall::PostLogOutJson(){
    QJsonObject logoutJsonObj;
    logoutJsonObj.insert("mac_address",GetMacAddress());
    logoutJsonObj.insert("username",GetCurrentUsername());
    logoutJsonObj.insert("activity_id",2);
    logoutJsonObj.insert("activitytime",GetDateTimeInString("yyyy-MM-dd HH:mm:ss"));
    QJsonDocument loginJsonDoc;
    loginJsonDoc.setObject(logoutJsonObj);


    QEventLoop eventLoop;
    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));



    // the HTTP request
    QNetworkRequest req( QUrl( QString("http://10.0.0.239:8765/users-activities/add") ) );
    req.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/json"));
    mgr.post(req,loginJsonDoc.toJson());
    eventLoop.exec(); // blocks stack until "finished()" has been called
}

void apicall::PostSystemDetails(){
    QJsonObject systemDetailsJsonObj;
    QJsonObject configurationJsonObj;
    configurationJsonObj.insert("ram",GetRamSize());
    configurationJsonObj.insert("OS",GetOperatingSystem());
    configurationJsonObj.insert("cpu",GetCpuConfig());
    systemDetailsJsonObj.insert("local_ip",GetLocalIp());
    systemDetailsJsonObj.insert("public_ip",GetPublicIp());
    systemDetailsJsonObj.insert("mac_address",GetMacAddress());
    systemDetailsJsonObj.insert("configuration",configurationJsonObj);
    QJsonDocument systemDetailsJsonDoc;
    systemDetailsJsonDoc.setObject(systemDetailsJsonObj);
    QEventLoop eventLoop;

    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    // the HTTP request
    QNetworkRequest req( QUrl( QString("http://10.0.0.239:8765/systems-details/add") ) );
    req.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/json"));
    mgr.post(req,systemDetailsJsonDoc.toJson());
    eventLoop.exec(); // blocks stack until "finished()" has been called
}
