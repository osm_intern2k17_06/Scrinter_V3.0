#ifndef APICALL_H
#define APICALL_H
#include <QString>
#include <QDir>
#include <QDateTime>
#include <QPixmap>
#include <QSize>
#include <QScreen>
#include <QApplication>
#include <QTextStream>
#include "constants.h"
#include <QSysInfo>
#include <QProcess>
#include <QString>
#include <QHostInfo>
#include <QHostAddress>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkInterface>
#include <QTcpSocket>
#include <QUrl>
#include <QUrlQuery>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>
#include <QTimer>
#ifdef WIN32
    #include <Windows.h>
#endif

class apicall
{
public:
    static QString GetDateTimeInString(QString strFormat = FILE_LOG_DATE_FORMAT);
    static QString GetCpuConfig();
    static QString GetLocalIp();
    static QString GetOperatingSystem();
    static QString GetPublicIp();
    static QString GetRamSize();
    static QString GetCurrentUsername();
    static QString GetMacAddress();
    static void PostSystemDetails();
    static void PostLogInJson();
    static void PostLogOutJson();

};

#endif // APICALL_H
