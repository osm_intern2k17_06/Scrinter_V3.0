#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QDialog>
#include <QComboBox>
#include "screencapsettings.h"
#include <QHotkey>
#include <QDir>
#include <QStandardPaths>
namespace Ui {
class AppSettings;
}

class AppSettings : public QDialog
{
    Q_OBJECT

public:
    explicit AppSettings(QWidget *parent);
    void setupUI(ScreenCapSettings *settings);
    ScreenCapSettings* GetUpdatedSettings(ScreenCapSettings *settings);
    void addItemsToQualityDdl(QComboBox *cmbBox, ScreenCapSettings *settings);
    void addItemsToFormatDdl(QComboBox *cmbBox, ScreenCapSettings *settings);
    QHotkey *hotkey_1;
    ~AppSettings();

protected:
    void done(int result);

private:


    Ui::AppSettings *ui;
    bool checkSettings();
    void dimensionError();

private slots:
    void setShortcut_1(const QKeySequence &sequence);
    void on_HotKeyConfirm_toggled(bool checked);
    void on_chkStartCapturing_toggled(bool checked);
};

#endif // APPSETTINGS_H
