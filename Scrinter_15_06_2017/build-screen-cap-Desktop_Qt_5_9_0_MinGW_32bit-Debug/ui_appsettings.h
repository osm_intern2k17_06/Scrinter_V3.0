/********************************************************************************
** Form generated from reading UI file 'appsettings.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_APPSETTINGS_H
#define UI_APPSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AppSettings
{
public:
    QDialogButtonBox *btnBox;
    QGroupBox *groupBox;
    QFormLayout *formLayout_3;
    QFrame *frame;
    QFormLayout *formLayout;
    QLabel *lblImgDimensions;
    QHBoxLayout *horizontalLayout;
    QLineEdit *txtImageDimension;
    QLabel *lblSummaryReport_2;
    QComboBox *ddlImgFormat;
    QLabel *label;
    QComboBox *ddlQuality;
    QGroupBox *groupBox_2;
    QWidget *layoutWidget;
    QFormLayout *formLayout_2;
    QLabel *lblMinimizeToTray;
    QCheckBox *chkMinimizeToTray;
    QLabel *lblStartMinimized;
    QCheckBox *chkStartMinimized;
    QLabel *lblStartCapturing;
    QCheckBox *chkStartCapturing;
    QFrame *line;
    QFrame *line_2;

    void setupUi(QDialog *AppSettings)
    {
        if (AppSettings->objectName().isEmpty())
            AppSettings->setObjectName(QStringLiteral("AppSettings"));
        AppSettings->resize(291, 330);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(AppSettings->sizePolicy().hasHeightForWidth());
        AppSettings->setSizePolicy(sizePolicy);
        AppSettings->setMinimumSize(QSize(291, 330));
        AppSettings->setMaximumSize(QSize(291, 330));
        QFont font;
        font.setFamily(QStringLiteral("Monospace"));
        AppSettings->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/img/img/Desktop.png"), QSize(), QIcon::Normal, QIcon::Off);
        AppSettings->setWindowIcon(icon);
        AppSettings->setModal(false);
        btnBox = new QDialogButtonBox(AppSettings);
        btnBox->setObjectName(QStringLiteral("btnBox"));
        btnBox->setGeometry(QRect(60, 287, 166, 22));
        btnBox->setOrientation(Qt::Horizontal);
        btnBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        groupBox = new QGroupBox(AppSettings);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setEnabled(true);
        groupBox->setGeometry(QRect(20, 147, 251, 121));
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        groupBox->setStyleSheet(QStringLiteral(""));
        groupBox->setFlat(false);
        formLayout_3 = new QFormLayout(groupBox);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        frame = new QFrame(groupBox);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);

        formLayout_3->setWidget(0, QFormLayout::LabelRole, frame);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        lblImgDimensions = new QLabel(groupBox);
        lblImgDimensions->setObjectName(QStringLiteral("lblImgDimensions"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lblImgDimensions);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        txtImageDimension = new QLineEdit(groupBox);
        txtImageDimension->setObjectName(QStringLiteral("txtImageDimension"));
        txtImageDimension->setMaxLength(3);

        horizontalLayout->addWidget(txtImageDimension);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);

        lblSummaryReport_2 = new QLabel(groupBox);
        lblSummaryReport_2->setObjectName(QStringLiteral("lblSummaryReport_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, lblSummaryReport_2);

        ddlImgFormat = new QComboBox(groupBox);
        ddlImgFormat->setObjectName(QStringLiteral("ddlImgFormat"));
        ddlImgFormat->setEditable(false);

        formLayout->setWidget(1, QFormLayout::FieldRole, ddlImgFormat);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label);

        ddlQuality = new QComboBox(groupBox);
        ddlQuality->setObjectName(QStringLiteral("ddlQuality"));

        formLayout->setWidget(2, QFormLayout::FieldRole, ddlQuality);


        formLayout_3->setLayout(0, QFormLayout::FieldRole, formLayout);

        groupBox_2 = new QGroupBox(AppSettings);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(20, 15, 251, 111));
        groupBox_2->setStyleSheet(QStringLiteral(""));
        layoutWidget = new QWidget(groupBox_2);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 30, 214, 74));
        formLayout_2 = new QFormLayout(layoutWidget);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        lblMinimizeToTray = new QLabel(layoutWidget);
        lblMinimizeToTray->setObjectName(QStringLiteral("lblMinimizeToTray"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, lblMinimizeToTray);

        chkMinimizeToTray = new QCheckBox(layoutWidget);
        chkMinimizeToTray->setObjectName(QStringLiteral("chkMinimizeToTray"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(chkMinimizeToTray->sizePolicy().hasHeightForWidth());
        chkMinimizeToTray->setSizePolicy(sizePolicy1);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, chkMinimizeToTray);

        lblStartMinimized = new QLabel(layoutWidget);
        lblStartMinimized->setObjectName(QStringLiteral("lblStartMinimized"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, lblStartMinimized);

        chkStartMinimized = new QCheckBox(layoutWidget);
        chkStartMinimized->setObjectName(QStringLiteral("chkStartMinimized"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(chkStartMinimized->sizePolicy().hasHeightForWidth());
        chkStartMinimized->setSizePolicy(sizePolicy2);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, chkStartMinimized);

        lblStartCapturing = new QLabel(layoutWidget);
        lblStartCapturing->setObjectName(QStringLiteral("lblStartCapturing"));
        lblStartCapturing->setMinimumSize(QSize(185, 0));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, lblStartCapturing);

        chkStartCapturing = new QCheckBox(layoutWidget);
        chkStartCapturing->setObjectName(QStringLiteral("chkStartCapturing"));
        sizePolicy2.setHeightForWidth(chkStartCapturing->sizePolicy().hasHeightForWidth());
        chkStartCapturing->setSizePolicy(sizePolicy2);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, chkStartCapturing);

        line = new QFrame(AppSettings);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(20, 123, 251, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(AppSettings);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(20, 260, 251, 20));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
#ifndef QT_NO_SHORTCUT
        lblMinimizeToTray->setBuddy(chkMinimizeToTray);
        lblStartMinimized->setBuddy(chkStartMinimized);
#endif // QT_NO_SHORTCUT

        retranslateUi(AppSettings);
        QObject::connect(btnBox, SIGNAL(accepted()), AppSettings, SLOT(accept()));
        QObject::connect(btnBox, SIGNAL(rejected()), AppSettings, SLOT(reject()));

        QMetaObject::connectSlotsByName(AppSettings);
    } // setupUi

    void retranslateUi(QDialog *AppSettings)
    {
        AppSettings->setWindowTitle(QApplication::translate("AppSettings", "Settings", Q_NULLPTR));
#ifndef QT_NO_STATUSTIP
        AppSettings->setStatusTip(QApplication::translate("AppSettings", "Settings for the application", Q_NULLPTR));
#endif // QT_NO_STATUSTIP
        groupBox->setTitle(QApplication::translate("AppSettings", "Image Settings", Q_NULLPTR));
        lblImgDimensions->setText(QApplication::translate("AppSettings", "Dimensions", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txtImageDimension->setToolTip(QApplication::translate("AppSettings", "Please enter a number between 50 to 100", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        txtImageDimension->setInputMask(QApplication::translate("AppSettings", "000", Q_NULLPTR));
        txtImageDimension->setPlaceholderText(QApplication::translate("AppSettings", "In percentage", Q_NULLPTR));
        lblSummaryReport_2->setText(QApplication::translate("AppSettings", "Format", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        ddlImgFormat->setToolTip(QApplication::translate("AppSettings", "PNG format will result in bigger file sizes.", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("AppSettings", "Quality", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        ddlQuality->setToolTip(QApplication::translate("AppSettings", "Reducing quality will reduce image size too", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        groupBox_2->setTitle(QApplication::translate("AppSettings", "General", Q_NULLPTR));
        lblMinimizeToTray->setText(QApplication::translate("AppSettings", "Minimize to system tray", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        chkMinimizeToTray->setToolTip(QApplication::translate("AppSettings", "Minimize to system tray instead of the taskbar...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        chkMinimizeToTray->setText(QString());
        lblStartMinimized->setText(QApplication::translate("AppSettings", "Start minimized", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        chkStartMinimized->setToolTip(QApplication::translate("AppSettings", "Minimize the application on start...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        chkStartMinimized->setText(QString());
        lblStartCapturing->setText(QApplication::translate("AppSettings", "Start capturing on startup", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        chkStartCapturing->setToolTip(QApplication::translate("AppSettings", "Start capturing snapshots on application startup.", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        chkStartCapturing->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class AppSettings: public Ui_AppSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_APPSETTINGS_H
