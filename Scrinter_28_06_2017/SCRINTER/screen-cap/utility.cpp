#include "utility.h"

bool Utility::CheckAndCreateFolder(QString folder, QString path) {
    QString fullFolderPath = QDir::cleanPath(path + QDir::separator() + folder);
    if(!QDir(fullFolderPath).exists()) {
        return QDir().mkdir(fullFolderPath);
    } else {
        return true;
    }
}

int Utility::GetRandomBetween(int min, int max) {
    return rand() % (max - min + 1) + min;
}

QString Utility::GetDateTimeInString(QString strFormat) {
    QDateTime now = QDateTime::currentDateTime();
    return now.toString(strFormat);
}


bool Utility::WriteLog(QString filePath, QString message, int line, QString srcfileName) {
    QString folderPath =  QDir::cleanPath(QDir::currentPath() + QDir::separator() + "logs");
    if(CheckAndCreateFolder(QDir::currentPath(), "logs")) {
        QString fullWritePath = QDir::cleanPath(folderPath + filePath);
        QFile file(fullWritePath);
        file.open(QIODevice::Append);
        QTextStream out(&file);
        out << "[" + GetDateTimeInString() + "][" + srcfileName + " : " + line + "] " + message;
        file.close();
        return true;
    }
    return false;
}
QString Utility::GetCpuConfig(){
    return QSysInfo::currentCpuArchitecture();
}

QString Utility::GetLocalIp(){

    QString localIPAddr;
    QTcpSocket socket;
    socket.connectToHost("8.8.8.8", 53);
    if (socket.waitForConnected()){
        localIPAddr = socket.localAddress().toString();
    }
    return localIPAddr;
}
QString Utility::GetOperatingSystem(){
    return QSysInfo::prettyProductName();
}

QString Utility::GetPublicIp(){
    QEventLoop eventLoop;
    QString publicIPAddr;
    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    // the HTTP request
    QNetworkRequest req( QUrl( QString("http://bot.whatismyipaddress.com/") ) );
    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec(); // blocks stack until "finished()" has been called

    if (reply->error() == QNetworkReply::NoError) {
        //success
        publicIPAddr =reply->readAll();
        delete reply;
    }
    else {
        //failure
        publicIPAddr =reply->errorString();
        delete reply;
    }
    return publicIPAddr;
}

QString Utility::GetRamSize(){
#ifdef _WIN32
    MEMORYSTATUSEX memory_status;
    ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    if (GlobalMemoryStatusEx(&memory_status)) {
        return QString("%1 MB").arg(memory_status.ullTotalPhys / (1024 * 1024));
    } else {
        return "Unknown RAM";
    }

#elif linux
    QProcess p;
    p.start("awk", QStringList() << "/MemTotal/ { print $2 }" << "/proc/meminfo");
    p.waitForFinished();
    QString memory = p.readAllStandardOutput();
    return QString("%1 MB").arg(memory.toLong() / 1024);
    p.close();

#elif __APPLE__
    QProcess p;
    p.start("sysctl", QStringList() << "kern.version" << "hw.physmem");
    p.waitForFinished();
    QString system_info = p.readAllStandardOutput();
    return system_info;
    p.close();

#endif

}

QString Utility:: GetCurrentUsername(){
    return QSysInfo::machineHostName();
}

