#include "mainwindow.h"
#include <QApplication>
#include <QSharedMemory>



int main(int argc, char *argv[])
{    
    QApplication a(argc, argv);

        const char* MEM_KEY = "42";

        QSharedMemory sharedMem(MEM_KEY);

        if (sharedMem.create(1024)) {
            qDebug() << "Create shared memory";
        } else {
            if (sharedMem.error() == QSharedMemory::AlreadyExists) {
                qWarning() << "Already create. Exiting process";
                return 1;
            } else {
                 // handle other possible errors
            }
        }
    MainWindow w;
    QApplication::setQuitOnLastWindowClosed(false);
    w.show();

    return a.exec();
}
