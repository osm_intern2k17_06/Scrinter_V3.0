#include "utility.h"
/**
 * @brief Utility::CheckAndCreateFolder
 * @param folder
 * @param path
 * @return
 * To check and create the folder with the time and date stamp
 */
bool Utility::CheckAndCreateFolder(QString folder, QString path)
{
    QString fullFolderPath = QDir::cleanPath(path + QDir::separator() + folder);
    if (!QDir(fullFolderPath).exists()) {
        return QDir().mkdir(fullFolderPath);
    }
    else {
        return true;
    }
}

int Utility::GetRandomBetween(int min, int max)
{
    return rand() % (max - min + 1) + min;
}

QString Utility::GetDateTimeInString(QString strFormat)
{
    QDateTime now = QDateTime::currentDateTime();
    return now.toString(strFormat);
}

/**
 * @brief Utility::WriteLog
 * @param filePath
 * @param message
 * @param line
 * @param srcfileName
 * @return to write the log of events
 */
bool Utility::WriteLog(QString filePath, QString message, int line, QString srcfileName)
{
    QString folderPath = QDir::cleanPath(QDir::currentPath() + QDir::separator() + "logs");
    if (CheckAndCreateFolder(QDir::currentPath(), "logs")) {
        QString fullWritePath = QDir::cleanPath(folderPath + filePath);
        QFile file(fullWritePath);
        file.open(QIODevice::Append);
        QTextStream out(&file);
        out << "[" + GetDateTimeInString() + "][" + srcfileName + " : " + line + "] " + message;
        file.close();
        return true;
    }
    return false;
}
//To check and create default folders
void Utility::CheckAndCreateDefaultScreenshotDir()
{
#ifdef __APPLE__
    QDir defaultScreenShotsFolder(QDir(GetMacApplicationPath() + DEF_FILE_PATH));
#else
    QDir defaultScreenShotsFolder(QDir(QCoreApplication::applicationDirPath() + DEF_FILE_PATH));
#endif
    if (!defaultScreenShotsFolder.exists()) {
        defaultScreenShotsFolder.mkdir(".");
    }
}
//To get the Scrinter.app directory path in mac os
#ifdef __APPLE__
QString Utility::GetMacApplicationPath()
{
    QDir Curr_Dir = QCoreApplication::applicationDirPath();
    Curr_Dir.cdUp();
    Curr_Dir.cdUp();
    Curr_Dir.cdUp();
    QString absolutePath = Curr_Dir.absolutePath();
    // absolutePath will contain a "/" at the end,
    // but we want the clean path to the .app bundle
    if (absolutePath.length() > 0 && absolutePath.right(1) == "/") {
        absolutePath.chop(1);
    }
    return absolutePath;
}
#endif
