#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFont>
#include <QHotkey>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QCloseEvent>
#include "constants.h"
#include "utility.h"
#include "screencapsettings.h"
#include "appsettings.h"
#include "apicall.h"
#include <QDesktopServices>
#include <QTemporaryDir>
#include <QResource>

namespace Ui {
class QApplication;
class QDesktopWidget;
class MainWindow;
class Utility;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent* event);
    void changeEvent(QEvent* event);

private:
    void changeStatusCtrls(bool isStartCapturing);
    QString getFullFilePath(QString filename);
    bool takeScreenshotAndSave(QString savePath, int windowId, QString imageFormat,
        int imgQuality, int imgHeight, int imgWidth);
    void updateSettings();
    bool settingsHasError();
    void folderCreationError();
    void createTrayIcons();
    void createTrayActions();
    void setCurrentImgHeightAndWidth();

    QHotkey* hotkeyMain;
    Ui::MainWindow* ui;
    AppSettings* appSettings;
    apicall* newapicall;
    ScreenCapSettings* settings;
    QTimer* timer;
    QTimer* randomTimer;
    QSize* qSize;

    QAction* minimizeAction;
    QAction* restoreAction;
    QAction* quitAction;

    QSystemTrayIcon* trayIcon;
    QMenu* trayIconMenu;
    int imgHeight;
    int imgWidth;
    enum appState { CAPTURING,
        STOPPED,
        QUITTING };
    int currAppState;

private slots:
    //void on_btnSettings_clicked();
    void on_btnStartCapture_clicked();
    void on_btnStopCapture_clicked();
    void timeToTakeScreenshot();
    void resetRandomTimer();
    void on_btnDestinationBrowser_clicked();
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void on_applicationQuit();
    //void on_btnAbout_clicked();
    //void setShortcut_1(const QKeySequence &sequence);
    void hotkeyActivated();
    //void on_HotKeyConfirm_toggled(bool checked);
    void on_btnAbout_2_triggered();
    void on_actionSettings_2_triggered();
    void on_actionExit_triggered();
    void on_actionclose_triggered();
    void on_actionHelp_2_triggered();
    void on_txtDestinationPath_editingFinished();
    void on_txtCaptureTime_editingFinished();
    void on_actionAbout_Qt_triggered();
};

#endif // MAINWINDOW_H
