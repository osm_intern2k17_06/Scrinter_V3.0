/********************************************************************************
** Form generated from reading UI file 'hottestwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOTTESTWIDGET_H
#define UI_HOTTESTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QKeySequenceEdit>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HotTestWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *playTab;
    QGridLayout *gridLayout;
    QCheckBox *hotkeyCheckbox_1;
    QCheckBox *hotkeyCheckbox_2;
    QLCDNumber *hotkeyCount_1;
    QKeySequenceEdit *hotkeySequenceEdit_1;
    QLabel *label;
    QToolButton *resetButton_1;
    QLCDNumber *hotkeyCount_2;
    QCheckBox *hotkeyCheckbox_3;
    QKeySequenceEdit *hotkeySequenceEdit_2;
    QLabel *label_3;
    QToolButton *resetButton_2;
    QCheckBox *hotkeyCheckbox_4;
    QKeySequenceEdit *hotkeySequenceEdit_3;
    QLCDNumber *hotkeyCount_3;
    QLabel *label_2;
    QToolButton *resetButton_3;
    QLabel *label_5;
    QKeySequenceEdit *hotkeySequenceEdit_4;
    QToolButton *resetButton_4;
    QLCDNumber *hotkeyCount_4;
    QCheckBox *hotkeyCheckbox_5;
    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_4;
    QLCDNumber *hotkeyCount_5;
    QToolButton *resetButton_5;
    QKeySequenceEdit *hotkeySequenceEdit_5;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_5;
    QSpacerItem *verticalSpacer_4;
    QWidget *testTab;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_6;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *hotkeyFLabel;
    QCheckBox *hotkeyFCheckBox;
    QLabel *hotkeyCtrlAltMetaF12Label;
    QCheckBox *hotkeyCtrlAltMetaF12CheckBox;
    QLabel *hotkeyCtrlShiftCancelLabel;
    QCheckBox *hotkeyCtrlShiftCancelCheckBox;
    QLabel *hotkeyMetaDelLabel;
    QCheckBox *hotkeyMetaDelCheckBox;
    QLabel *hotkeyNumlockLabel;
    QCheckBox *hotkeyNumlockCheckBox;
    QLabel *hotkeyCtrl5Label;
    QCheckBox *hotkeyCtrl5CheckBox;
    QLabel *hotkeyShiftTabLabel;
    QCheckBox *hotkeyShiftTabCheckBox;
    QLabel *hotkeyShiftLabel;
    QCheckBox *hotkeyShiftCheckBox;
    QLabel *hotkeyShiftLabel_2;
    QCheckBox *hotkeyShiftCheckBox_2;
    QLabel *hotkeyShiftAltKLabel;
    QCheckBox *hotkeyShiftAltKCheckBox;
    QLabel *hotkeyShiftAltKLabel_2;
    QCheckBox *hotkeyShiftAltKCheckBox_2;
    QSpacerItem *verticalSpacer_6;
    QWidget *threadTab;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_7;
    QCheckBox *threadEnableCheckBox;
    QSpacerItem *verticalSpacer_7;
    QWidget *nativeTab;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_8;
    QFormLayout *formLayout_2;
    QLabel *keyLabel;
    QSpinBox *nativeKeySpinBox;
    QLabel *modifiersLabel;
    QSpinBox *nativeModifiersSpinBox;
    QLabel *countLabel;
    QLCDNumber *nativeCount;
    QLabel *registeredLabel;
    QCheckBox *registeredCheckBox;

    void setupUi(QWidget *HotTestWidget)
    {
        if (HotTestWidget->objectName().isEmpty())
            HotTestWidget->setObjectName(QStringLiteral("HotTestWidget"));
        HotTestWidget->resize(520, 426);
        verticalLayout = new QVBoxLayout(HotTestWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(HotTestWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        playTab = new QWidget();
        playTab->setObjectName(QStringLiteral("playTab"));
        gridLayout = new QGridLayout(playTab);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        hotkeyCheckbox_1 = new QCheckBox(playTab);
        hotkeyCheckbox_1->setObjectName(QStringLiteral("hotkeyCheckbox_1"));

        gridLayout->addWidget(hotkeyCheckbox_1, 0, 0, 1, 1);

        hotkeyCheckbox_2 = new QCheckBox(playTab);
        hotkeyCheckbox_2->setObjectName(QStringLiteral("hotkeyCheckbox_2"));

        gridLayout->addWidget(hotkeyCheckbox_2, 1, 0, 1, 1);

        hotkeyCount_1 = new QLCDNumber(playTab);
        hotkeyCount_1->setObjectName(QStringLiteral("hotkeyCount_1"));
        hotkeyCount_1->setEnabled(false);
        QPalette palette;
        QBrush brush(QColor(85, 255, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 255, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        QBrush brush2(QColor(0, 0, 0, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        QBrush brush3(QColor(120, 120, 120, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        hotkeyCount_1->setPalette(palette);
        hotkeyCount_1->setAutoFillBackground(true);
        hotkeyCount_1->setFrameShape(QFrame::Panel);
        hotkeyCount_1->setFrameShadow(QFrame::Sunken);
        hotkeyCount_1->setSegmentStyle(QLCDNumber::Flat);

        gridLayout->addWidget(hotkeyCount_1, 0, 3, 1, 1);

        hotkeySequenceEdit_1 = new QKeySequenceEdit(playTab);
        hotkeySequenceEdit_1->setObjectName(QStringLiteral("hotkeySequenceEdit_1"));

        gridLayout->addWidget(hotkeySequenceEdit_1, 0, 1, 1, 1);

        label = new QLabel(playTab);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 2, 1, 1);

        resetButton_1 = new QToolButton(playTab);
        resetButton_1->setObjectName(QStringLiteral("resetButton_1"));
        resetButton_1->setArrowType(Qt::LeftArrow);

        gridLayout->addWidget(resetButton_1, 0, 4, 1, 1);

        hotkeyCount_2 = new QLCDNumber(playTab);
        hotkeyCount_2->setObjectName(QStringLiteral("hotkeyCount_2"));
        hotkeyCount_2->setEnabled(false);
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        hotkeyCount_2->setPalette(palette1);
        hotkeyCount_2->setAutoFillBackground(true);
        hotkeyCount_2->setFrameShape(QFrame::Panel);
        hotkeyCount_2->setFrameShadow(QFrame::Sunken);
        hotkeyCount_2->setSegmentStyle(QLCDNumber::Flat);

        gridLayout->addWidget(hotkeyCount_2, 1, 3, 1, 1);

        hotkeyCheckbox_3 = new QCheckBox(playTab);
        hotkeyCheckbox_3->setObjectName(QStringLiteral("hotkeyCheckbox_3"));

        gridLayout->addWidget(hotkeyCheckbox_3, 2, 0, 1, 1);

        hotkeySequenceEdit_2 = new QKeySequenceEdit(playTab);
        hotkeySequenceEdit_2->setObjectName(QStringLiteral("hotkeySequenceEdit_2"));

        gridLayout->addWidget(hotkeySequenceEdit_2, 1, 1, 1, 1);

        label_3 = new QLabel(playTab);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 1, 2, 1, 1);

        resetButton_2 = new QToolButton(playTab);
        resetButton_2->setObjectName(QStringLiteral("resetButton_2"));
        resetButton_2->setArrowType(Qt::LeftArrow);

        gridLayout->addWidget(resetButton_2, 1, 4, 1, 1);

        hotkeyCheckbox_4 = new QCheckBox(playTab);
        hotkeyCheckbox_4->setObjectName(QStringLiteral("hotkeyCheckbox_4"));

        gridLayout->addWidget(hotkeyCheckbox_4, 3, 0, 1, 1);

        hotkeySequenceEdit_3 = new QKeySequenceEdit(playTab);
        hotkeySequenceEdit_3->setObjectName(QStringLiteral("hotkeySequenceEdit_3"));

        gridLayout->addWidget(hotkeySequenceEdit_3, 2, 1, 1, 1);

        hotkeyCount_3 = new QLCDNumber(playTab);
        hotkeyCount_3->setObjectName(QStringLiteral("hotkeyCount_3"));
        hotkeyCount_3->setEnabled(false);
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        hotkeyCount_3->setPalette(palette2);
        hotkeyCount_3->setAutoFillBackground(true);
        hotkeyCount_3->setFrameShape(QFrame::Panel);
        hotkeyCount_3->setFrameShadow(QFrame::Sunken);
        hotkeyCount_3->setSegmentStyle(QLCDNumber::Flat);

        gridLayout->addWidget(hotkeyCount_3, 2, 3, 1, 1);

        label_2 = new QLabel(playTab);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 2, 2, 1, 1);

        resetButton_3 = new QToolButton(playTab);
        resetButton_3->setObjectName(QStringLiteral("resetButton_3"));
        resetButton_3->setArrowType(Qt::LeftArrow);

        gridLayout->addWidget(resetButton_3, 2, 4, 1, 1);

        label_5 = new QLabel(playTab);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 3, 2, 1, 1);

        hotkeySequenceEdit_4 = new QKeySequenceEdit(playTab);
        hotkeySequenceEdit_4->setObjectName(QStringLiteral("hotkeySequenceEdit_4"));

        gridLayout->addWidget(hotkeySequenceEdit_4, 3, 1, 1, 1);

        resetButton_4 = new QToolButton(playTab);
        resetButton_4->setObjectName(QStringLiteral("resetButton_4"));
        resetButton_4->setArrowType(Qt::LeftArrow);

        gridLayout->addWidget(resetButton_4, 3, 4, 1, 1);

        hotkeyCount_4 = new QLCDNumber(playTab);
        hotkeyCount_4->setObjectName(QStringLiteral("hotkeyCount_4"));
        hotkeyCount_4->setEnabled(false);
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        hotkeyCount_4->setPalette(palette3);
        hotkeyCount_4->setAutoFillBackground(true);
        hotkeyCount_4->setFrameShape(QFrame::Panel);
        hotkeyCount_4->setFrameShadow(QFrame::Sunken);
        hotkeyCount_4->setSegmentStyle(QLCDNumber::Flat);

        gridLayout->addWidget(hotkeyCount_4, 3, 3, 1, 1);

        hotkeyCheckbox_5 = new QCheckBox(playTab);
        hotkeyCheckbox_5->setObjectName(QStringLiteral("hotkeyCheckbox_5"));

        gridLayout->addWidget(hotkeyCheckbox_5, 4, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 5, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 5, 1, 1, 1);

        label_4 = new QLabel(playTab);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 4, 2, 1, 1);

        hotkeyCount_5 = new QLCDNumber(playTab);
        hotkeyCount_5->setObjectName(QStringLiteral("hotkeyCount_5"));
        hotkeyCount_5->setEnabled(false);
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        hotkeyCount_5->setPalette(palette4);
        hotkeyCount_5->setAutoFillBackground(true);
        hotkeyCount_5->setFrameShape(QFrame::Panel);
        hotkeyCount_5->setFrameShadow(QFrame::Sunken);
        hotkeyCount_5->setSegmentStyle(QLCDNumber::Flat);

        gridLayout->addWidget(hotkeyCount_5, 4, 3, 1, 1);

        resetButton_5 = new QToolButton(playTab);
        resetButton_5->setObjectName(QStringLiteral("resetButton_5"));
        resetButton_5->setArrowType(Qt::LeftArrow);

        gridLayout->addWidget(resetButton_5, 4, 4, 1, 1);

        hotkeySequenceEdit_5 = new QKeySequenceEdit(playTab);
        hotkeySequenceEdit_5->setObjectName(QStringLiteral("hotkeySequenceEdit_5"));

        gridLayout->addWidget(hotkeySequenceEdit_5, 4, 1, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 5, 2, 1, 1);

        verticalSpacer_5 = new QSpacerItem(20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_5, 5, 4, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 5, 3, 1, 1);

        tabWidget->addTab(playTab, QString());
        testTab = new QWidget();
        testTab->setObjectName(QStringLiteral("testTab"));
        verticalLayout_2 = new QVBoxLayout(testTab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_6 = new QLabel(testTab);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setWordWrap(true);

        verticalLayout_2->addWidget(label_6);

        groupBox = new QGroupBox(testTab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setCheckable(true);
        groupBox->setChecked(false);
        formLayout = new QFormLayout(groupBox);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        hotkeyFLabel = new QLabel(groupBox);
        hotkeyFLabel->setObjectName(QStringLiteral("hotkeyFLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, hotkeyFLabel);

        hotkeyFCheckBox = new QCheckBox(groupBox);
        hotkeyFCheckBox->setObjectName(QStringLiteral("hotkeyFCheckBox"));

        formLayout->setWidget(0, QFormLayout::FieldRole, hotkeyFCheckBox);

        hotkeyCtrlAltMetaF12Label = new QLabel(groupBox);
        hotkeyCtrlAltMetaF12Label->setObjectName(QStringLiteral("hotkeyCtrlAltMetaF12Label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, hotkeyCtrlAltMetaF12Label);

        hotkeyCtrlAltMetaF12CheckBox = new QCheckBox(groupBox);
        hotkeyCtrlAltMetaF12CheckBox->setObjectName(QStringLiteral("hotkeyCtrlAltMetaF12CheckBox"));

        formLayout->setWidget(1, QFormLayout::FieldRole, hotkeyCtrlAltMetaF12CheckBox);

        hotkeyCtrlShiftCancelLabel = new QLabel(groupBox);
        hotkeyCtrlShiftCancelLabel->setObjectName(QStringLiteral("hotkeyCtrlShiftCancelLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, hotkeyCtrlShiftCancelLabel);

        hotkeyCtrlShiftCancelCheckBox = new QCheckBox(groupBox);
        hotkeyCtrlShiftCancelCheckBox->setObjectName(QStringLiteral("hotkeyCtrlShiftCancelCheckBox"));

        formLayout->setWidget(2, QFormLayout::FieldRole, hotkeyCtrlShiftCancelCheckBox);

        hotkeyMetaDelLabel = new QLabel(groupBox);
        hotkeyMetaDelLabel->setObjectName(QStringLiteral("hotkeyMetaDelLabel"));

        formLayout->setWidget(3, QFormLayout::LabelRole, hotkeyMetaDelLabel);

        hotkeyMetaDelCheckBox = new QCheckBox(groupBox);
        hotkeyMetaDelCheckBox->setObjectName(QStringLiteral("hotkeyMetaDelCheckBox"));

        formLayout->setWidget(3, QFormLayout::FieldRole, hotkeyMetaDelCheckBox);

        hotkeyNumlockLabel = new QLabel(groupBox);
        hotkeyNumlockLabel->setObjectName(QStringLiteral("hotkeyNumlockLabel"));

        formLayout->setWidget(4, QFormLayout::LabelRole, hotkeyNumlockLabel);

        hotkeyNumlockCheckBox = new QCheckBox(groupBox);
        hotkeyNumlockCheckBox->setObjectName(QStringLiteral("hotkeyNumlockCheckBox"));

        formLayout->setWidget(4, QFormLayout::FieldRole, hotkeyNumlockCheckBox);

        hotkeyCtrl5Label = new QLabel(groupBox);
        hotkeyCtrl5Label->setObjectName(QStringLiteral("hotkeyCtrl5Label"));

        formLayout->setWidget(5, QFormLayout::LabelRole, hotkeyCtrl5Label);

        hotkeyCtrl5CheckBox = new QCheckBox(groupBox);
        hotkeyCtrl5CheckBox->setObjectName(QStringLiteral("hotkeyCtrl5CheckBox"));

        formLayout->setWidget(5, QFormLayout::FieldRole, hotkeyCtrl5CheckBox);

        hotkeyShiftTabLabel = new QLabel(groupBox);
        hotkeyShiftTabLabel->setObjectName(QStringLiteral("hotkeyShiftTabLabel"));

        formLayout->setWidget(6, QFormLayout::LabelRole, hotkeyShiftTabLabel);

        hotkeyShiftTabCheckBox = new QCheckBox(groupBox);
        hotkeyShiftTabCheckBox->setObjectName(QStringLiteral("hotkeyShiftTabCheckBox"));

        formLayout->setWidget(6, QFormLayout::FieldRole, hotkeyShiftTabCheckBox);

        hotkeyShiftLabel = new QLabel(groupBox);
        hotkeyShiftLabel->setObjectName(QStringLiteral("hotkeyShiftLabel"));

        formLayout->setWidget(7, QFormLayout::LabelRole, hotkeyShiftLabel);

        hotkeyShiftCheckBox = new QCheckBox(groupBox);
        hotkeyShiftCheckBox->setObjectName(QStringLiteral("hotkeyShiftCheckBox"));

        formLayout->setWidget(7, QFormLayout::FieldRole, hotkeyShiftCheckBox);

        hotkeyShiftLabel_2 = new QLabel(groupBox);
        hotkeyShiftLabel_2->setObjectName(QStringLiteral("hotkeyShiftLabel_2"));

        formLayout->setWidget(8, QFormLayout::LabelRole, hotkeyShiftLabel_2);

        hotkeyShiftCheckBox_2 = new QCheckBox(groupBox);
        hotkeyShiftCheckBox_2->setObjectName(QStringLiteral("hotkeyShiftCheckBox_2"));

        formLayout->setWidget(8, QFormLayout::FieldRole, hotkeyShiftCheckBox_2);

        hotkeyShiftAltKLabel = new QLabel(groupBox);
        hotkeyShiftAltKLabel->setObjectName(QStringLiteral("hotkeyShiftAltKLabel"));

        formLayout->setWidget(9, QFormLayout::LabelRole, hotkeyShiftAltKLabel);

        hotkeyShiftAltKCheckBox = new QCheckBox(groupBox);
        hotkeyShiftAltKCheckBox->setObjectName(QStringLiteral("hotkeyShiftAltKCheckBox"));

        formLayout->setWidget(9, QFormLayout::FieldRole, hotkeyShiftAltKCheckBox);

        hotkeyShiftAltKLabel_2 = new QLabel(groupBox);
        hotkeyShiftAltKLabel_2->setObjectName(QStringLiteral("hotkeyShiftAltKLabel_2"));

        formLayout->setWidget(10, QFormLayout::LabelRole, hotkeyShiftAltKLabel_2);

        hotkeyShiftAltKCheckBox_2 = new QCheckBox(groupBox);
        hotkeyShiftAltKCheckBox_2->setObjectName(QStringLiteral("hotkeyShiftAltKCheckBox_2"));

        formLayout->setWidget(10, QFormLayout::FieldRole, hotkeyShiftAltKCheckBox_2);


        verticalLayout_2->addWidget(groupBox);

        verticalSpacer_6 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_6);

        tabWidget->addTab(testTab, QString());
        threadTab = new QWidget();
        threadTab->setObjectName(QStringLiteral("threadTab"));
        verticalLayout_3 = new QVBoxLayout(threadTab);
        verticalLayout_3->setSpacing(12);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_7 = new QLabel(threadTab);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setTextFormat(Qt::RichText);
        label_7->setWordWrap(true);

        verticalLayout_3->addWidget(label_7);

        threadEnableCheckBox = new QCheckBox(threadTab);
        threadEnableCheckBox->setObjectName(QStringLiteral("threadEnableCheckBox"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        threadEnableCheckBox->setFont(font);

        verticalLayout_3->addWidget(threadEnableCheckBox);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_7);

        tabWidget->addTab(threadTab, QString());
        nativeTab = new QWidget();
        nativeTab->setObjectName(QStringLiteral("nativeTab"));
        verticalLayout_4 = new QVBoxLayout(nativeTab);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_8 = new QLabel(nativeTab);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setWordWrap(true);

        verticalLayout_4->addWidget(label_8);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        keyLabel = new QLabel(nativeTab);
        keyLabel->setObjectName(QStringLiteral("keyLabel"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, keyLabel);

        nativeKeySpinBox = new QSpinBox(nativeTab);
        nativeKeySpinBox->setObjectName(QStringLiteral("nativeKeySpinBox"));
        nativeKeySpinBox->setMaximum(999999999);
        nativeKeySpinBox->setDisplayIntegerBase(16);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, nativeKeySpinBox);

        modifiersLabel = new QLabel(nativeTab);
        modifiersLabel->setObjectName(QStringLiteral("modifiersLabel"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, modifiersLabel);

        nativeModifiersSpinBox = new QSpinBox(nativeTab);
        nativeModifiersSpinBox->setObjectName(QStringLiteral("nativeModifiersSpinBox"));
        nativeModifiersSpinBox->setMaximum(999999999);
        nativeModifiersSpinBox->setDisplayIntegerBase(16);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, nativeModifiersSpinBox);

        countLabel = new QLabel(nativeTab);
        countLabel->setObjectName(QStringLiteral("countLabel"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, countLabel);

        nativeCount = new QLCDNumber(nativeTab);
        nativeCount->setObjectName(QStringLiteral("nativeCount"));
        nativeCount->setEnabled(false);
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        nativeCount->setPalette(palette5);
        nativeCount->setAutoFillBackground(true);
        nativeCount->setFrameShape(QFrame::Panel);
        nativeCount->setFrameShadow(QFrame::Sunken);
        nativeCount->setSegmentStyle(QLCDNumber::Flat);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, nativeCount);

        registeredLabel = new QLabel(nativeTab);
        registeredLabel->setObjectName(QStringLiteral("registeredLabel"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, registeredLabel);

        registeredCheckBox = new QCheckBox(nativeTab);
        registeredCheckBox->setObjectName(QStringLiteral("registeredCheckBox"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, registeredCheckBox);


        verticalLayout_4->addLayout(formLayout_2);

        verticalLayout_4->setStretch(1, 1);
        tabWidget->addTab(nativeTab, QString());

        verticalLayout->addWidget(tabWidget);

        QWidget::setTabOrder(hotkeyCheckbox_1, hotkeySequenceEdit_1);
        QWidget::setTabOrder(hotkeySequenceEdit_1, resetButton_1);
        QWidget::setTabOrder(resetButton_1, hotkeyCheckbox_2);
        QWidget::setTabOrder(hotkeyCheckbox_2, hotkeySequenceEdit_2);
        QWidget::setTabOrder(hotkeySequenceEdit_2, resetButton_2);
        QWidget::setTabOrder(resetButton_2, hotkeyCheckbox_3);
        QWidget::setTabOrder(hotkeyCheckbox_3, hotkeySequenceEdit_3);
        QWidget::setTabOrder(hotkeySequenceEdit_3, resetButton_3);
        QWidget::setTabOrder(resetButton_3, hotkeyCheckbox_4);
        QWidget::setTabOrder(hotkeyCheckbox_4, hotkeySequenceEdit_4);
        QWidget::setTabOrder(hotkeySequenceEdit_4, resetButton_4);
        QWidget::setTabOrder(resetButton_4, hotkeyCheckbox_5);
        QWidget::setTabOrder(hotkeyCheckbox_5, hotkeySequenceEdit_5);
        QWidget::setTabOrder(hotkeySequenceEdit_5, resetButton_5);

        retranslateUi(HotTestWidget);
        QObject::connect(hotkeyCheckbox_1, SIGNAL(toggled(bool)), hotkeyCount_1, SLOT(setEnabled(bool)));
        QObject::connect(hotkeyCheckbox_2, SIGNAL(toggled(bool)), hotkeyCount_2, SLOT(setEnabled(bool)));
        QObject::connect(hotkeyCheckbox_3, SIGNAL(toggled(bool)), hotkeyCount_3, SLOT(setEnabled(bool)));
        QObject::connect(hotkeyCheckbox_4, SIGNAL(toggled(bool)), hotkeyCount_4, SLOT(setEnabled(bool)));
        QObject::connect(hotkeyCheckbox_5, SIGNAL(toggled(bool)), hotkeyCount_5, SLOT(setEnabled(bool)));
        QObject::connect(hotkeyCheckbox_1, SIGNAL(toggled(bool)), hotkeySequenceEdit_1, SLOT(setDisabled(bool)));
        QObject::connect(hotkeyCheckbox_2, SIGNAL(toggled(bool)), hotkeySequenceEdit_2, SLOT(setDisabled(bool)));
        QObject::connect(hotkeyCheckbox_3, SIGNAL(toggled(bool)), hotkeySequenceEdit_3, SLOT(setDisabled(bool)));
        QObject::connect(hotkeyCheckbox_4, SIGNAL(toggled(bool)), hotkeySequenceEdit_4, SLOT(setDisabled(bool)));
        QObject::connect(hotkeyCheckbox_5, SIGNAL(toggled(bool)), hotkeySequenceEdit_5, SLOT(setDisabled(bool)));
        QObject::connect(registeredCheckBox, SIGNAL(toggled(bool)), nativeCount, SLOT(setEnabled(bool)));
        QObject::connect(registeredCheckBox, SIGNAL(toggled(bool)), nativeModifiersSpinBox, SLOT(setDisabled(bool)));
        QObject::connect(registeredCheckBox, SIGNAL(toggled(bool)), nativeKeySpinBox, SLOT(setDisabled(bool)));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(HotTestWidget);
    } // setupUi

    void retranslateUi(QWidget *HotTestWidget)
    {
        HotTestWidget->setWindowTitle(QApplication::translate("HotTestWidget", "HotTestWidget", Q_NULLPTR));
        hotkeyCheckbox_1->setText(QApplication::translate("HotTestWidget", "Hotkey &1:", Q_NULLPTR));
        hotkeyCheckbox_2->setText(QApplication::translate("HotTestWidget", "Hotkey &2:", Q_NULLPTR));
        label->setText(QApplication::translate("HotTestWidget", "Count:", Q_NULLPTR));
        hotkeyCheckbox_3->setText(QApplication::translate("HotTestWidget", "Hotkey &3:", Q_NULLPTR));
        label_3->setText(QApplication::translate("HotTestWidget", "Count:", Q_NULLPTR));
        hotkeyCheckbox_4->setText(QApplication::translate("HotTestWidget", "Hotkey &4:", Q_NULLPTR));
        label_2->setText(QApplication::translate("HotTestWidget", "Count:", Q_NULLPTR));
        label_5->setText(QApplication::translate("HotTestWidget", "Count:", Q_NULLPTR));
        hotkeyCheckbox_5->setText(QApplication::translate("HotTestWidget", "Hotkey &5:", Q_NULLPTR));
        label_4->setText(QApplication::translate("HotTestWidget", "Count:", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(playTab), QApplication::translate("HotTestWidget", "Playground", Q_NULLPTR));
        label_6->setText(QApplication::translate("HotTestWidget", "<b>Testing:</b> Please press the combinations listed below to check whether they work properly or not. Everytime a shortcut is triggered, the checkbox will toggle it's value. Set the test active to begin.", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("HotTestWidget", "Test Active:", Q_NULLPTR));
        hotkeyFLabel->setText(QApplication::translate("HotTestWidget", "Hotkey: F", Q_NULLPTR));
        hotkeyCtrlAltMetaF12Label->setText(QApplication::translate("HotTestWidget", "Hotkey: ctrl+alt+meta+F12", Q_NULLPTR));
        hotkeyCtrlShiftCancelLabel->setText(QApplication::translate("HotTestWidget", "Hotkey: ctrl+shift+cancel", Q_NULLPTR));
        hotkeyMetaDelLabel->setText(QApplication::translate("HotTestWidget", "Hotkey: meta+del", Q_NULLPTR));
        hotkeyNumlockLabel->setText(QApplication::translate("HotTestWidget", "Hotkey: numlock", Q_NULLPTR));
        hotkeyCtrl5Label->setText(QApplication::translate("HotTestWidget", "Hotkey: ctrl+5", Q_NULLPTR));
        hotkeyShiftTabLabel->setText(QApplication::translate("HotTestWidget", "Hotkey: shift+Tab", Q_NULLPTR));
        hotkeyShiftLabel->setText(QApplication::translate("HotTestWidget", "Hotkey: shift+,", Q_NULLPTR));
        hotkeyShiftLabel_2->setText(QApplication::translate("HotTestWidget", "Hotkey: shift+;", Q_NULLPTR));
        hotkeyShiftAltKLabel->setText(QApplication::translate("HotTestWidget", "Hotkey: shift+alt+K", Q_NULLPTR));
        hotkeyShiftAltKLabel_2->setText(QApplication::translate("HotTestWidget", "Hotkey: shift+alt+K", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(testTab), QApplication::translate("HotTestWidget", "Testings", Q_NULLPTR));
        label_7->setText(QApplication::translate("HotTestWidget", "<html><head/><body><p>This test was designed to try out multi-threaded shortcuts. The QHotkey class is completly <span style=\" font-weight:600;\">threadsafe</span>, but this test can help to see if it acutally works (It does).</p><p>If activated, <span style=\" font-style:italic;\">Hotkey 4 and Hotkey 5 </span>of the Playground will each run on their own thread. This means:</p><ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" text-decoration: underline;\">Mainthread:</span> Hotkey 1, 2, 3</li><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" text-decoration: underline;\">Second thread:</span> Hotkey 4</li><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><spa"
                        "n style=\" text-decoration: underline;\">Third thread:</span> Hotkey 5</li></ul><p><span style=\" font-weight:600;\">Note:</span> The two hotkeys will be moved to the threads. For simplicity-reasons, you can't move them back in this test (But its possible, just not done here). Restart the test to get them back.</p></body></html>", Q_NULLPTR));
        threadEnableCheckBox->setText(QApplication::translate("HotTestWidget", "Enable Threaded Hotkeys", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(threadTab), QApplication::translate("HotTestWidget", "Threading", Q_NULLPTR));
        label_8->setText(QApplication::translate("HotTestWidget", "<html><head/><body><p>QHotkey allows you to set native shortcuts explicitly. These, of course, only work on the platform they were choosen for. All platform use special constants for their key codes and modifiers, which makes it pretty simple to use them from code. If you want to test them out here, google for the tables.</p><p>In most cases, you will not need to specify native shortcuts directly. However, as explaind on previos tabs, some shotcuts may not be creatable from Qt's key (e.g. Numblock numbers). In that case, you can set the directly.</p><p><span style=\" text-decoration: underline;\">Example: Ctrl+A</span></p><ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Windows:</span> Key: <span style=\" font-style:italic;\">0x0041</span>, Modifier: <span style=\" font-style:italic;\">0x0002</span"
                        "></li><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">X11:</span> Key: <span style=\" font-style:italic;\">0x0026</span>, Modifier: <span style=\" font-style:italic;\">0x0004</span></li><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">OsX:</span> Key: <span style=\" font-style:italic;\">0x0000</span>, Modifier: <span style=\" font-style:italic;\">0x0100</span><span style=\" text-decoration: underline;\"><br/></span></li></ul></body></html>", Q_NULLPTR));
        keyLabel->setText(QApplication::translate("HotTestWidget", "Key:", Q_NULLPTR));
        nativeKeySpinBox->setPrefix(QApplication::translate("HotTestWidget", "0x", Q_NULLPTR));
        modifiersLabel->setText(QApplication::translate("HotTestWidget", "Modifiers:", Q_NULLPTR));
        nativeModifiersSpinBox->setPrefix(QApplication::translate("HotTestWidget", "0x", Q_NULLPTR));
        countLabel->setText(QApplication::translate("HotTestWidget", "Count:", Q_NULLPTR));
        registeredLabel->setText(QApplication::translate("HotTestWidget", "Registered:", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(nativeTab), QApplication::translate("HotTestWidget", "Native Shortcut", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class HotTestWidget: public Ui_HotTestWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOTTESTWIDGET_H
