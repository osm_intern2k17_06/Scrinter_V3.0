/****************************************************************************
** Meta object code from reading C++ file 'hottestwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../QHotkey/HotkeyTest/hottestwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'hottestwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_HotTestWidget_t {
    QByteArrayData data[23];
    char stringdata0[380];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_HotTestWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_HotTestWidget_t qt_meta_stringdata_HotTestWidget = {
    {
QT_MOC_LITERAL(0, 0, 13), // "HotTestWidget"
QT_MOC_LITERAL(1, 14, 13), // "setShortcut_1"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 8), // "sequence"
QT_MOC_LITERAL(4, 38, 13), // "setShortcut_2"
QT_MOC_LITERAL(5, 52, 13), // "setShortcut_3"
QT_MOC_LITERAL(6, 66, 13), // "setShortcut_4"
QT_MOC_LITERAL(7, 80, 13), // "setShortcut_5"
QT_MOC_LITERAL(8, 94, 10), // "increase_1"
QT_MOC_LITERAL(9, 105, 10), // "increase_2"
QT_MOC_LITERAL(10, 116, 10), // "increase_3"
QT_MOC_LITERAL(11, 127, 10), // "increase_4"
QT_MOC_LITERAL(12, 138, 10), // "increase_5"
QT_MOC_LITERAL(13, 149, 24), // "on_resetButton_1_clicked"
QT_MOC_LITERAL(14, 174, 24), // "on_resetButton_2_clicked"
QT_MOC_LITERAL(15, 199, 24), // "on_resetButton_3_clicked"
QT_MOC_LITERAL(16, 224, 24), // "on_resetButton_4_clicked"
QT_MOC_LITERAL(17, 249, 24), // "on_resetButton_5_clicked"
QT_MOC_LITERAL(18, 274, 19), // "on_groupBox_toggled"
QT_MOC_LITERAL(19, 294, 7), // "checked"
QT_MOC_LITERAL(20, 302, 31), // "on_threadEnableCheckBox_clicked"
QT_MOC_LITERAL(21, 334, 29), // "on_registeredCheckBox_toggled"
QT_MOC_LITERAL(22, 364, 15) // "increase_native"

    },
    "HotTestWidget\0setShortcut_1\0\0sequence\0"
    "setShortcut_2\0setShortcut_3\0setShortcut_4\0"
    "setShortcut_5\0increase_1\0increase_2\0"
    "increase_3\0increase_4\0increase_5\0"
    "on_resetButton_1_clicked\0"
    "on_resetButton_2_clicked\0"
    "on_resetButton_3_clicked\0"
    "on_resetButton_4_clicked\0"
    "on_resetButton_5_clicked\0on_groupBox_toggled\0"
    "checked\0on_threadEnableCheckBox_clicked\0"
    "on_registeredCheckBox_toggled\0"
    "increase_native"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_HotTestWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x08 /* Private */,
       4,    1,  112,    2, 0x08 /* Private */,
       5,    1,  115,    2, 0x08 /* Private */,
       6,    1,  118,    2, 0x08 /* Private */,
       7,    1,  121,    2, 0x08 /* Private */,
       8,    0,  124,    2, 0x08 /* Private */,
       9,    0,  125,    2, 0x08 /* Private */,
      10,    0,  126,    2, 0x08 /* Private */,
      11,    0,  127,    2, 0x08 /* Private */,
      12,    0,  128,    2, 0x08 /* Private */,
      13,    0,  129,    2, 0x08 /* Private */,
      14,    0,  130,    2, 0x08 /* Private */,
      15,    0,  131,    2, 0x08 /* Private */,
      16,    0,  132,    2, 0x08 /* Private */,
      17,    0,  133,    2, 0x08 /* Private */,
      18,    1,  134,    2, 0x08 /* Private */,
      20,    0,  137,    2, 0x08 /* Private */,
      21,    1,  138,    2, 0x08 /* Private */,
      22,    0,  141,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QKeySequence,    3,
    QMetaType::Void, QMetaType::QKeySequence,    3,
    QMetaType::Void, QMetaType::QKeySequence,    3,
    QMetaType::Void, QMetaType::QKeySequence,    3,
    QMetaType::Void, QMetaType::QKeySequence,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   19,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   19,
    QMetaType::Void,

       0        // eod
};

void HotTestWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        HotTestWidget *_t = static_cast<HotTestWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setShortcut_1((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 1: _t->setShortcut_2((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 2: _t->setShortcut_3((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 3: _t->setShortcut_4((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 4: _t->setShortcut_5((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 5: _t->increase_1(); break;
        case 6: _t->increase_2(); break;
        case 7: _t->increase_3(); break;
        case 8: _t->increase_4(); break;
        case 9: _t->increase_5(); break;
        case 10: _t->on_resetButton_1_clicked(); break;
        case 11: _t->on_resetButton_2_clicked(); break;
        case 12: _t->on_resetButton_3_clicked(); break;
        case 13: _t->on_resetButton_4_clicked(); break;
        case 14: _t->on_resetButton_5_clicked(); break;
        case 15: _t->on_groupBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->on_threadEnableCheckBox_clicked(); break;
        case 17: _t->on_registeredCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->increase_native(); break;
        default: ;
        }
    }
}

const QMetaObject HotTestWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_HotTestWidget.data,
      qt_meta_data_HotTestWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *HotTestWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *HotTestWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_HotTestWidget.stringdata0))
        return static_cast<void*>(const_cast< HotTestWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int HotTestWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
