#include "mainwindow.h"
#ifdef __APPLE__
#include "utility.h"
#endif
#include <QApplication>
#ifdef _WIN32
#include <QSharedMemory>
#include <QMessageBox>
#else
#include <QDir>
#include <QLockFile>
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //to set the application Icon
QApplication::setWindowIcon(QIcon(":/img/img/Desktop.png"));

//To set the application as a single instance applcication in Windows
#ifdef _WIN32
    const char* MEM_KEY = "42";

    QSharedMemory sharedMem(MEM_KEY);

    if (sharedMem.create(1024)) {
        qDebug() << "Create shared memory";
    } else {
        if (sharedMem.error() == QSharedMemory::AlreadyExists) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText("You already have this app running."
                           "\r\nOnly one instance is allowed.");
            msgBox.exec();
            return 1;
        } else {
            // handle other possible errors
        }
    }
    //To set the application as a single instance applcication in Linux and Mac Osx
#else
    QString tmpDir = QDir::tempPath();
    QLockFile lockFile(tmpDir + "/Scrinter.lock");

    if(!lockFile.tryLock(100)){
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("You already have this app running."
                       "\r\nOnly one instance is allowed.");
        msgBox.exec();
        return 1;
    }
#endif
    MainWindow w;
    QApplication::setQuitOnLastWindowClosed(false);
    w.show();

    return a.exec();
}
