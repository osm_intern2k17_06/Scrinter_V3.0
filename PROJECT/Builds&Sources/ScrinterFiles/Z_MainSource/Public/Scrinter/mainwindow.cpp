#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <QDir>
#include <QTimer>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QStandardPaths>
#include <QHotkey>
#include <QMessageBox>

class SCUtility;

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , hotkeyMain(new QHotkey(this))
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Utility::CheckAndCreateDefaultScreenshotDir();
#ifdef _WIN32
    this->setFixedWidth(552);
    this->setFixedHeight(120);
#elif __APPLE__
    this->setFixedWidth(605);
    this->setFixedHeight(127);
#elif linux
    this->setFixedWidth(589);
    this->setFixedHeight(112);
#endif
    this->settings = new ScreenCapSettings();
    if (!this->settings->ValidateSettings()) {
        if (!this->settingsHasError()) {
            this->close();
            qApp->exit();
        }
    }
    hotkeyMain = new QHotkey(this->settings->GetHotkeySequence());
    hotkeyMain->setRegistered(true);
    connect(hotkeyMain, &QHotkey::activated, this, &MainWindow::hotkeyActivated);
    ui->txtCaptureTime->setText(QString::number(this->settings->GetCapTime()));
    ui->txtDestinationPath->setText(this->settings->GetFilePath());
    ui->chkRandomCapture->setChecked(this->settings->GetCapIsRandom());
    this->timer = new QTimer(this);
    this->randomTimer = new QTimer(this);    
    if (this->settings->GetCaptureOnStartup() != DEF_CAP_STARTUP) {
        MainWindow::on_btnStartCapture_clicked();
    }
    else if (this->settings->GetCapIsOn() == DEF_CAP_IS_ON) {
        // Is not on
        emit on_btnStartCapture_clicked();
    }
    else {
        // Is on
        emit on_btnStopCapture_clicked();
    }
    // Initialize the actions
    createTrayActions();
    // Add the actions to the tray icon menu
    createTrayIcons();
    trayIcon->setIcon(QIcon(":/img/img/Desktop.png"));
    trayIcon->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    this->updateSettings();
    this->settings->SaveMainSettings();
    if (trayIcon->isVisible()) {
        hide();
        event->ignore();
    }
}

/**
 * @brief MainWindow::on_btnStartCapture_clicked
 * Fires when the user clicks on the capture button.
 * 1. Changes the status of the application.
 * 2. Calls update settings
 * 3. Sets the image width and height based on the setting.
 * 4. Starts the timer and connects the SLOT to MainWindow::timeToTakeScreenshot()
 */
void MainWindow::on_btnStartCapture_clicked()
{
    currAppState = CAPTURING;
    if (this->settings->GetStartMinimized()) {
        this->setWindowState(Qt::WindowMinimized);
    }
    //QWidget::showMinimized();
    this->changeStatusCtrls(true);
    this->updateSettings();
    this->setCurrentImgHeightAndWidth();
    this->timer->start(this->settings->GetCapTime() * 1000);
    if (this->settings->GetCapIsRandom()) {
        /******************************************************************
         * 1. Checking if random is set, if so starting the random timer
         * 2. Attaching the screenshot to the random timer
         * 3. Attaching the restart of random timer to the main timer
         ******************************************************************/
        this->randomTimer->start(Utility::GetRandomBetween(1, this->settings->GetCapTime() - 1) * 1000);
        connect(this->randomTimer, SIGNAL(timeout()), this, SLOT(timeToTakeScreenshot()));
        connect(timer, SIGNAL(timeout()), this, SLOT(resetRandomTimer()));
    }
    else {
        // No random, setting screenshot to random timer.
        connect(timer, SIGNAL(timeout()), this, SLOT(timeToTakeScreenshot()));
    }
}
/**
 * @brief MainWindow::on_btnStopCapture_clicked
 * To stop taking screenshots
 */
void MainWindow::on_btnStopCapture_clicked()
{
    currAppState = STOPPED;
    timeToTakeScreenshot();
    this->changeStatusCtrls(false);
}

void MainWindow::changeStatusCtrls(bool isStartCapturing = true)
{
    ui->btnStartCapture->setEnabled(!isStartCapturing);
    ui->actionSettings_2->setEnabled(!isStartCapturing);
    ui->btnDestinationBrowser->setEnabled(!isStartCapturing);
    ui->actionSettings_2->setEnabled(!isStartCapturing);
    ui->txtCaptureTime->setEnabled(!isStartCapturing);
    ui->txtDestinationPath->setEnabled(!isStartCapturing);
    ui->chkRandomCapture->setEnabled(!isStartCapturing);
    // Stop button is opposite.
    ui->btnStopCapture->setEnabled(isStartCapturing);
    if (!isStartCapturing) {
        this->timer->stop();
    }
}
/**
 * @brief MainWindow::timeToTakeScreenshot
 * To start the process of taking screenshot and save it the folder with date and time stamp
 */
void MainWindow::timeToTakeScreenshot()
{
    if (!QDir(this->settings->GetFilePath()).exists()) {
        QMessageBox::information(this, "Error", "Destination folder missing, destination path is set to default path.", "Ok");
#ifdef __APPLE__
        settings->SetFilePath(Utility::GetMacApplicationPath() + "/Scrinter_Screenshots");
#else
        settings->SetFilePath(QCoreApplication::applicationDirPath() + "/Scrinter_Screenshots");
#endif
        return;
    }
    if (this->settings->GetCapIsRandom() && this->randomTimer && this->randomTimer->isActive()) {
        this->randomTimer->stop();
    }
    QString fileName = Utility::GetDateTimeInString(FILE_DATE_FORMAT);
    if (currAppState == STOPPED) {
        fileName = "STOPPING_" + fileName;
    }
    else if (currAppState == QUITTING) {
        fileName = "QUITTING_" + fileName;
    }
    QString fullPath = this->getFullFilePath(fileName);
    if (fullPath.isEmpty() && currAppState == CAPTURING) {
        // Don't show error on quit or on stopping.
        this->folderCreationError();
        return;
    }
    bool isSuccess = this->takeScreenshotAndSave(fullPath,
        QApplication::desktop()->winId(), this->settings->GetImgFormat(),
        this->settings->GetImgQuality(), this->imgHeight, this->imgWidth);
    if (!isSuccess) {
        Utility::WriteLog(fileName, "Error while taking a screenshot.", __LINE__, __FILE__);
    }
}
/**
 * @brief MainWindow::getFullFilePath
 * @param filename
 * @return Returns full file path of the screenshots destination
 */
QString MainWindow::getFullFilePath(QString filename)
{
    QString dateFoldername = Utility::GetDateTimeInString(FOLDER_DATE_FORMAT);
    QString filepath = this->settings->GetFilePath();
    if (filepath.trimmed().isEmpty() || !Utility::CheckAndCreateFolder(dateFoldername, this->settings->GetFilePath())) {
        emit on_btnStopCapture_clicked();
        return "";
    }
    QString filePath = this->settings->GetFilePath();
    return QDir::cleanPath(filePath + QDir::separator() + dateFoldername + QDir::separator() + filename);
}
/**
 * @brief MainWindow::takeScreenshotAndSave
 * @param savePath
 * @param windowId
 * @param imageFormat
 * @param imgQuality
 * @param imgHeight
 * @param imgWidth
 * @return To take screenshot and save it
 */
bool MainWindow::takeScreenshotAndSave(QString savePath, int windowId, QString imageFormat,
    int imgQuality, int imgHeight, int imgWidth)
{
    QScreen* screen = QGuiApplication::primaryScreen();
    QPixmap originalPixmap = screen->grabWindow(windowId);
    originalPixmap = originalPixmap.scaled(imgWidth, imgHeight, Qt::KeepAspectRatio,
        Qt::SmoothTransformation);
    return originalPixmap.save(savePath + "." + imageFormat.toLower(), imageFormat.toUtf8(),
        imgQuality);
}
/**
 * @brief MainWindow::updateSettings
 *
 * To update the settings
 */
void MainWindow::updateSettings()
{
    this->settings->SetCapTime(this->ui->txtCaptureTime->text().toInt());
    this->settings->SetFilePath(this->ui->txtDestinationPath->text());
    this->settings->SetCapIsRandom(this->ui->chkRandomCapture->isChecked());
    if (ui->btnStartCapture->isEnabled()) {
        // Is off
        this->settings->SetCapIsOn(DEF_CAP_IS_OFF);
    }
    else {
        // Is on
        this->settings->SetCapIsOn(DEF_CAP_IS_ON);
    }
}
/**
 * @brief MainWindow::on_btnDestinationBrowser_clicked
 * To open the file dialog to choose the destination directory for the screenshots
 */
void MainWindow::on_btnDestinationBrowser_clicked()
{
    QString temporaryFilePath;
    temporaryFilePath = QFileDialog::getExistingDirectory(this, tr("Select a directory to save in..."),
        this->settings->GetFilePath(),
        QFileDialog::ShowDirsOnly
            | QFileDialog::DontResolveSymlinks);
    if (temporaryFilePath != "") {
        ui->txtDestinationPath->setText(temporaryFilePath);
        this->settings->SetFilePath(temporaryFilePath);
    }
    else {
        ui->txtDestinationPath->setText(this->settings->GetFilePath());
    }
}
/**
 * @brief MainWindow::settingsHasError
 * @return
 * check if settings couldn't load
 */
bool MainWindow::settingsHasError()
{
    QMessageBox msgBox;
    QFont font(DLG_FONT_FAMILY);
    font.setStyleHint(QFont::TypeWriter);
    msgBox.setWindowTitle("Settings Error");
    msgBox.setText("The settings for the application seem to be invalid.");
    msgBox.setInformativeText("Do you want to restore default settings or close the app?");
    msgBox.setStandardButtons(QMessageBox::RestoreDefaults | QMessageBox::Close);
    msgBox.setDefaultButton(QMessageBox::RestoreDefaults);
    msgBox.setFont(font);
    int ret = msgBox.exec();
    if (ret == QMessageBox::RestoreDefaults) {
        this->settings->LoadDefaultValues();
        return true;
    }
    else if (ret == QMessageBox::Close) {
        return false;
    }
    return false;
}
/**
 * @brief MainWindow::folderCreationError
 * To check for the folder creation error
 */
void MainWindow::folderCreationError()
{
    QMessageBox msgBox;
    QFont font(DLG_FONT_FAMILY);
    font.setStyleHint(QFont::TypeWriter);
    msgBox.setWindowTitle("Unable to create folder");
    msgBox.setText("We were unable to create a folder in the destination path.");
    msgBox.setInformativeText("Please ensure that we have read/write permissions to that folder. If the folder is over a network,"
                              " ensure that you are connected to it.");
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setFont(font);
    msgBox.exec();
}
/**
 * @brief MainWindow::resetRandomTimer
 * To reset the random time value
 */
void MainWindow::resetRandomTimer()
{
    if (this->settings->GetCapIsRandom()) {
        this->randomTimer->start(Utility::GetRandomBetween(1, this->settings->GetCapTime()) * 1000);
    }
}
/**
 * @brief MainWindow::createTrayIcons
 * to create icon in the tray
 */
void MainWindow::createTrayIcons()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);
    trayIconMenu->setEnabled(true);
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
        this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
}
/**
 * @brief MainWindow::createTrayActions
 * To create actions for the icon in the tray
 */
void MainWindow::createTrayActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), this, SLOT(on_applicationQuit()));
}
/**
 * @brief MainWindow::setCurrentImgHeightAndWidth
 * To set the current Image Height and width of the screenshot
 */
void MainWindow::setCurrentImgHeightAndWidth()
{
    QList<QScreen*> lstScreens = QApplication::screens();
    this->imgHeight = 0;
    this->imgWidth = 0;
    for (int i = 0; i < lstScreens.size(); ++i) {
        QSize qSize = lstScreens[i]->size();
        // Height will not be added
        if (this->imgHeight < qSize.height()) {
            this->imgHeight = qSize.height();
        }
        this->imgWidth += qSize.width();
    }
    int dimensionPercent = this->settings->GetCapDimensions();
    this->imgWidth = (this->imgWidth * dimensionPercent) / 100;
    this->imgHeight = (this->imgHeight * dimensionPercent) / 100;
}
/**
 * @brief MainWindow::changeEvent
 * @param event
 * Customizing the minimize even for the hidden mode/stealth mode
 */
void MainWindow::changeEvent(QEvent* event)
{
    QMainWindow::changeEvent(event);
    if (event->type() == QEvent::WindowStateChange && windowState() == Qt::WindowMinimized
        && this->isVisible()) {
        if (this->settings->GetHiddenMode()) {
            hide();
            trayIcon->setVisible(false);
        }
        else {
            showMinimized();
            trayIcon->setVisible(true);
        }
    }
    else if (event->type() == QEvent::ApplicationActivate) {
        show();
    }
}
/**
 * @brief MainWindow::iconActivated
 * @param reason
 * If the icon is activated the window showed be show if minimized or hidden
 */
void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        break;
    case QSystemTrayIcon::DoubleClick:
        if (this->isHidden()) {
            this->showNormal();
            this->activateWindow();
        }
        else {
            this->hide();
        }
        break;
    default:;
    }
}
/**
 * @brief MainWindow::on_applicationQuit
 * for exiting the application
 */
void MainWindow::on_applicationQuit()
{
    // Show the dialog box.
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Quitting...", "Are you sure you want to quit?",
        QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        currAppState = QUITTING;
        if (!ui->btnStartCapture->isEnabled()) {
            // If it is running.
            timeToTakeScreenshot();
            this->updateSettings();
            this->settings->SaveMainSettings();
        }
        qApp->quit();
    }
}
/**
 * @brief MainWindow::hotkeyActivated
 * Handle the shortcut events
 */
void MainWindow::hotkeyActivated()
{

    if ((MainWindow::isMinimized() || MainWindow::isHidden()) == false) {
        if (this->settings->GetHiddenMode() == true) {
            MainWindow::hide();
        }
        else {
            MainWindow::showMinimized();
        }
    }
    else {
        MainWindow::showNormal();
        MainWindow::activateWindow();
#ifdef __APPLE__
        MainWindow::raise();
#endif
    }
}
/**
 * @brief MainWindow::on_btnAbout_2_triggered
 * When About is click in the Help menu
 */
void MainWindow::on_btnAbout_2_triggered()
{
    QMessageBox aboutMessageBox;
    aboutMessageBox.setWindowIcon(this->windowIcon());
    aboutMessageBox.setWindowTitle("About Scrinter");
    aboutMessageBox.setText("Simple cross platform screen capturing utility, that can be used for monitoring by capturing screen-shots randomly every X seconds.\n\nBuilt with <3 at Osmosys Software Solutions.");
    aboutMessageBox.exec();
}
/**
 * @brief MainWindow::on_btnSettings_clicked
 * Fires on the click on the settings button.
 */
void MainWindow::on_actionSettings_2_triggered()
{
    AppSettings* appSettings = new AppSettings(this);
    appSettings->setupUI(this->settings);
    appSettings->exec();
    if (appSettings->result() == QDialog::Accepted) {
        this->settings = appSettings->GetUpdatedSettings(this->settings);
    }
    hotkeyMain->setRegistered(false);
    hotkeyMain = new QHotkey(this->settings->GetHotkeySequence());
    hotkeyMain->setRegistered(true);
    connect(hotkeyMain, &QHotkey::activated, this, &MainWindow::hotkeyActivated);
    this->updateSettings();
    this->settings->SaveMainSettings();
}
/**
 * @brief MainWindow::on_actionExit_triggered
 * when exit is clicked in file menu
 */
void MainWindow::on_actionExit_triggered()
{
    this->updateSettings();
    this->settings->SaveMainSettings();
    MainWindow::on_applicationQuit();
}
/**
 * @brief MainWindow::on_actionclose_triggered
 * when close is pressed in file menu
 */
void MainWindow::on_actionclose_triggered()
{
    this->updateSettings();
    this->settings->SaveMainSettings();
    if (trayIcon->isVisible()) {
        hide();
    }
    else {
        showMinimized();
    }
}
/**
 * @brief MainWindow::on_actionHelp_2_triggered
 * when Help is clicked in help menu
 */
void MainWindow::on_actionHelp_2_triggered()
{
#ifdef Q_OS_MAC
    QDesktopServices::openUrl(QUrl("file://" + Utility::GetMacApplicationPath() + "/Help/help.html"));
#else
    QDesktopServices::openUrl(QUrl(QDir::currentPath() + "/Help/help.html"));
#endif
}

void MainWindow::on_txtDestinationPath_editingFinished()
{
    if (!QDir(ui->txtDestinationPath->text()).exists()) {
        QMessageBox::information(this, "Invalid Input", "Please enter a valid directory path.", "Ok");
        if (QDir(settings->GetFilePath()).exists()) {
            ui->txtDestinationPath->setText(settings->GetFilePath());
        }
        else {
#ifdef Q_OS_MAC
            ui->txtDestinationPath->setText(Utility::GetMacApplicationPath() + "/Scrinter_Screenshots");
#else
            ui->txtDestinationPath->setText(QCoreApplication::applicationDirPath() + "/Scrinter_Screenshots");
#endif
        }
    }
}
/**
 * @brief MainWindow::on_txtCaptureTime_editingFinished
 * for time period validation
 */
void MainWindow::on_txtCaptureTime_editingFinished()
{
    if (ui->txtCaptureTime->text().isEmpty()) {
        QMessageBox::information(this, "Invalid Input", "Please enter a valid Input.", "Ok");
        ui->txtCaptureTime->setText(QString::number(this->settings->GetCapTime()));
    }
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    QMessageBox::aboutQt(this);
}
