/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *cntrlMainWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *hLayout_2;
    QLabel *lblDestination;
    QSpacerItem *hSpacer_5;
    QLineEdit *txtDestinationPath;
    QPushButton *btnDestinationBrowser;
    QHBoxLayout *hLayout_1;
    QHBoxLayout *horizontalLayout_3;
    QCheckBox *chkRandomCapture;
    QSpacerItem *hSpacer_12;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lblCaptureTime;
    QSpacerItem *hSpacer_6;
    QLineEdit *txtCaptureTime;
    QSpacerItem *hSpacer_7;
    QLabel *lblTmpSeconds;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnAbout;
    QPushButton *btnSettings;
    QSpacerItem *hSpacer_11;
    QPushButton *btnStartCapture;
    QPushButton *btnStopCapture;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(775, 353);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(515, 107));
        QFont font;
        font.setFamily(QStringLiteral("Monospace"));
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/img/img/Desktop.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setWindowOpacity(1);
        MainWindow->setDockNestingEnabled(false);
        MainWindow->setUnifiedTitleAndToolBarOnMac(true);
        cntrlMainWidget = new QWidget(MainWindow);
        cntrlMainWidget->setObjectName(QStringLiteral("cntrlMainWidget"));
        cntrlMainWidget->setMinimumSize(QSize(540, 0));
        layoutWidget = new QWidget(cntrlMainWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 70, 761, 171));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(10, 0, 0, 0);
        hLayout_2 = new QHBoxLayout();
        hLayout_2->setSpacing(6);
        hLayout_2->setObjectName(QStringLiteral("hLayout_2"));
        hLayout_2->setSizeConstraint(QLayout::SetMinAndMaxSize);
        lblDestination = new QLabel(layoutWidget);
        lblDestination->setObjectName(QStringLiteral("lblDestination"));

        hLayout_2->addWidget(lblDestination);

        hSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hLayout_2->addItem(hSpacer_5);

        txtDestinationPath = new QLineEdit(layoutWidget);
        txtDestinationPath->setObjectName(QStringLiteral("txtDestinationPath"));

        hLayout_2->addWidget(txtDestinationPath);

        btnDestinationBrowser = new QPushButton(layoutWidget);
        btnDestinationBrowser->setObjectName(QStringLiteral("btnDestinationBrowser"));
        btnDestinationBrowser->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btnDestinationBrowser->sizePolicy().hasHeightForWidth());
        btnDestinationBrowser->setSizePolicy(sizePolicy1);
        btnDestinationBrowser->setMinimumSize(QSize(20, 20));
        btnDestinationBrowser->setBaseSize(QSize(20, 20));
        QIcon icon1;
        icon1.addFile(QStringLiteral("img/Spotlight Blue Button1.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnDestinationBrowser->setIcon(icon1);

        hLayout_2->addWidget(btnDestinationBrowser);


        verticalLayout->addLayout(hLayout_2);

        hLayout_1 = new QHBoxLayout();
        hLayout_1->setSpacing(6);
        hLayout_1->setObjectName(QStringLiteral("hLayout_1"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        chkRandomCapture = new QCheckBox(layoutWidget);
        chkRandomCapture->setObjectName(QStringLiteral("chkRandomCapture"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(chkRandomCapture->sizePolicy().hasHeightForWidth());
        chkRandomCapture->setSizePolicy(sizePolicy2);
        chkRandomCapture->setMinimumSize(QSize(111, 0));
        chkRandomCapture->setFont(font);
        chkRandomCapture->setAcceptDrops(false);
        chkRandomCapture->setLayoutDirection(Qt::LeftToRight);

        horizontalLayout_3->addWidget(chkRandomCapture);

        hSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(hSpacer_12);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lblCaptureTime = new QLabel(layoutWidget);
        lblCaptureTime->setObjectName(QStringLiteral("lblCaptureTime"));
        sizePolicy2.setHeightForWidth(lblCaptureTime->sizePolicy().hasHeightForWidth());
        lblCaptureTime->setSizePolicy(sizePolicy2);

        horizontalLayout_2->addWidget(lblCaptureTime);

        hSpacer_6 = new QSpacerItem(17, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(hSpacer_6);

        txtCaptureTime = new QLineEdit(layoutWidget);
        txtCaptureTime->setObjectName(QStringLiteral("txtCaptureTime"));
        sizePolicy1.setHeightForWidth(txtCaptureTime->sizePolicy().hasHeightForWidth());
        txtCaptureTime->setSizePolicy(sizePolicy1);
        txtCaptureTime->setMinimumSize(QSize(23, 28));
        txtCaptureTime->setAcceptDrops(true);
        txtCaptureTime->setInputMethodHints(Qt::ImhDigitsOnly);

        horizontalLayout_2->addWidget(txtCaptureTime);

        hSpacer_7 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(hSpacer_7);

        lblTmpSeconds = new QLabel(layoutWidget);
        lblTmpSeconds->setObjectName(QStringLiteral("lblTmpSeconds"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Maximum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(lblTmpSeconds->sizePolicy().hasHeightForWidth());
        lblTmpSeconds->setSizePolicy(sizePolicy3);
        lblTmpSeconds->setMinimumSize(QSize(0, 24));

        horizontalLayout_2->addWidget(lblTmpSeconds);


        horizontalLayout_3->addLayout(horizontalLayout_2);


        hLayout_1->addLayout(horizontalLayout_3);


        verticalLayout->addLayout(hLayout_1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btnAbout = new QPushButton(layoutWidget);
        btnAbout->setObjectName(QStringLiteral("btnAbout"));
        btnAbout->setEnabled(true);
        sizePolicy1.setHeightForWidth(btnAbout->sizePolicy().hasHeightForWidth());
        btnAbout->setSizePolicy(sizePolicy1);
        btnAbout->setMinimumSize(QSize(110, 0));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/img/img/Get Info Blue Button.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnAbout->setIcon(icon2);
        btnAbout->setIconSize(QSize(24, 24));

        horizontalLayout->addWidget(btnAbout);

        btnSettings = new QPushButton(layoutWidget);
        btnSettings->setObjectName(QStringLiteral("btnSettings"));
        sizePolicy1.setHeightForWidth(btnSettings->sizePolicy().hasHeightForWidth());
        btnSettings->setSizePolicy(sizePolicy1);
        btnSettings->setMinimumSize(QSize(110, 0));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/img/img/Gear.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnSettings->setIcon(icon3);
        btnSettings->setIconSize(QSize(24, 24));

        horizontalLayout->addWidget(btnSettings);

        hSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(hSpacer_11);

        btnStartCapture = new QPushButton(layoutWidget);
        btnStartCapture->setObjectName(QStringLiteral("btnStartCapture"));
        sizePolicy2.setHeightForWidth(btnStartCapture->sizePolicy().hasHeightForWidth());
        btnStartCapture->setSizePolicy(sizePolicy2);
        btnStartCapture->setMinimumSize(QSize(110, 0));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/img/img/Play.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStartCapture->setIcon(icon4);
        btnStartCapture->setIconSize(QSize(24, 24));
        btnStartCapture->setFlat(false);

        horizontalLayout->addWidget(btnStartCapture);

        btnStopCapture = new QPushButton(layoutWidget);
        btnStopCapture->setObjectName(QStringLiteral("btnStopCapture"));
        QSizePolicy sizePolicy4(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(110);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(btnStopCapture->sizePolicy().hasHeightForWidth());
        btnStopCapture->setSizePolicy(sizePolicy4);
        btnStopCapture->setMinimumSize(QSize(110, 0));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/img/img/Stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStopCapture->setIcon(icon5);
        btnStopCapture->setIconSize(QSize(24, 24));

        horizontalLayout->addWidget(btnStopCapture);


        verticalLayout->addLayout(horizontalLayout);

        MainWindow->setCentralWidget(cntrlMainWidget);
#ifndef QT_NO_SHORTCUT
        lblDestination->setBuddy(txtDestinationPath);
        lblCaptureTime->setBuddy(txtCaptureTime);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(txtDestinationPath, btnDestinationBrowser);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Scrinter", Q_NULLPTR));
        lblDestination->setText(QApplication::translate("MainWindow", "Folder Path", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txtDestinationPath->setToolTip(QApplication::translate("MainWindow", "Folder to save the screenshots in...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btnDestinationBrowser->setToolTip(QApplication::translate("MainWindow", "Browse...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnDestinationBrowser->setText(QApplication::translate("MainWindow", "Browse", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        chkRandomCapture->setToolTip(QApplication::translate("MainWindow", "Take screenshots at random...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        chkRandomCapture->setText(QApplication::translate("MainWindow", "at random", Q_NULLPTR));
        lblCaptureTime->setText(QApplication::translate("MainWindow", "Time Period", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txtCaptureTime->setToolTip(QApplication::translate("MainWindow", "How often to take screenshots?", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        lblTmpSeconds->setText(QApplication::translate("MainWindow", "seconds,", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnAbout->setToolTip(QApplication::translate("MainWindow", "Software used by the app..", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnAbout->setText(QApplication::translate("MainWindow", "About", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnSettings->setToolTip(QApplication::translate("MainWindow", "Settings...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnSettings->setText(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnStartCapture->setToolTip(QApplication::translate("MainWindow", "Start Capturing...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnStartCapture->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnStopCapture->setToolTip(QApplication::translate("MainWindow", "Stop Capturing...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnStopCapture->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
