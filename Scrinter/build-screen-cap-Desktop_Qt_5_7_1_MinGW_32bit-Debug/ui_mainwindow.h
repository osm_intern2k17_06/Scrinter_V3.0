/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *cntrlMainWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *hLayout_1;
    QLabel *lblCaptureTime;
    QSpacerItem *hSpacer_1;
    QLineEdit *txtCaptureTime;
    QSpacerItem *hSpacer_2;
    QLabel *lblTmpSeconds;
    QCheckBox *chkRandomCapture;
    QHBoxLayout *hLayout_2;
    QLabel *lblDestination;
    QSpacerItem *hSpacer_5;
    QLineEdit *txtDestinationPath;
    QPushButton *btnDestinationBrowser;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnStartCapture;
    QPushButton *btnStopCapture;
    QSpacerItem *hSpacer_11;
    QPushButton *btnSettings;
    QPushButton *btnAbout;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(515, 107);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(515, 107));
        MainWindow->setMaximumSize(QSize(540, 107));
        QFont font;
        font.setFamily(QStringLiteral("Monospace"));
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/img/img/Desktop.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setWindowOpacity(1);
        MainWindow->setDockNestingEnabled(false);
        MainWindow->setUnifiedTitleAndToolBarOnMac(true);
        cntrlMainWidget = new QWidget(MainWindow);
        cntrlMainWidget->setObjectName(QStringLiteral("cntrlMainWidget"));
        cntrlMainWidget->setMinimumSize(QSize(540, 0));
        layoutWidget = new QWidget(cntrlMainWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(8, 8, 498, 91));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        hLayout_1 = new QHBoxLayout();
        hLayout_1->setSpacing(6);
        hLayout_1->setObjectName(QStringLiteral("hLayout_1"));
        lblCaptureTime = new QLabel(layoutWidget);
        lblCaptureTime->setObjectName(QStringLiteral("lblCaptureTime"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lblCaptureTime->sizePolicy().hasHeightForWidth());
        lblCaptureTime->setSizePolicy(sizePolicy1);

        hLayout_1->addWidget(lblCaptureTime);

        hSpacer_1 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hLayout_1->addItem(hSpacer_1);

        txtCaptureTime = new QLineEdit(layoutWidget);
        txtCaptureTime->setObjectName(QStringLiteral("txtCaptureTime"));
        sizePolicy1.setHeightForWidth(txtCaptureTime->sizePolicy().hasHeightForWidth());
        txtCaptureTime->setSizePolicy(sizePolicy1);
        txtCaptureTime->setAcceptDrops(true);
        txtCaptureTime->setInputMethodHints(Qt::ImhDigitsOnly);

        hLayout_1->addWidget(txtCaptureTime);

        hSpacer_2 = new QSpacerItem(5, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hLayout_1->addItem(hSpacer_2);

        lblTmpSeconds = new QLabel(layoutWidget);
        lblTmpSeconds->setObjectName(QStringLiteral("lblTmpSeconds"));
        sizePolicy1.setHeightForWidth(lblTmpSeconds->sizePolicy().hasHeightForWidth());
        lblTmpSeconds->setSizePolicy(sizePolicy1);

        hLayout_1->addWidget(lblTmpSeconds);

        chkRandomCapture = new QCheckBox(layoutWidget);
        chkRandomCapture->setObjectName(QStringLiteral("chkRandomCapture"));
        sizePolicy1.setHeightForWidth(chkRandomCapture->sizePolicy().hasHeightForWidth());
        chkRandomCapture->setSizePolicy(sizePolicy1);
        chkRandomCapture->setFont(font);
        chkRandomCapture->setAcceptDrops(false);
        chkRandomCapture->setLayoutDirection(Qt::RightToLeft);

        hLayout_1->addWidget(chkRandomCapture);


        verticalLayout->addLayout(hLayout_1);

        hLayout_2 = new QHBoxLayout();
        hLayout_2->setSpacing(6);
        hLayout_2->setObjectName(QStringLiteral("hLayout_2"));
        hLayout_2->setSizeConstraint(QLayout::SetMinAndMaxSize);
        lblDestination = new QLabel(layoutWidget);
        lblDestination->setObjectName(QStringLiteral("lblDestination"));

        hLayout_2->addWidget(lblDestination);

        hSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hLayout_2->addItem(hSpacer_5);

        txtDestinationPath = new QLineEdit(layoutWidget);
        txtDestinationPath->setObjectName(QStringLiteral("txtDestinationPath"));

        hLayout_2->addWidget(txtDestinationPath);

        btnDestinationBrowser = new QPushButton(layoutWidget);
        btnDestinationBrowser->setObjectName(QStringLiteral("btnDestinationBrowser"));
        btnDestinationBrowser->setEnabled(true);
        sizePolicy.setHeightForWidth(btnDestinationBrowser->sizePolicy().hasHeightForWidth());
        btnDestinationBrowser->setSizePolicy(sizePolicy);
        btnDestinationBrowser->setMinimumSize(QSize(20, 20));
        btnDestinationBrowser->setBaseSize(QSize(20, 20));

        hLayout_2->addWidget(btnDestinationBrowser);


        verticalLayout->addLayout(hLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btnStartCapture = new QPushButton(layoutWidget);
        btnStartCapture->setObjectName(QStringLiteral("btnStartCapture"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/img/img/Play.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStartCapture->setIcon(icon1);
        btnStartCapture->setIconSize(QSize(24, 24));
        btnStartCapture->setFlat(false);

        horizontalLayout->addWidget(btnStartCapture);

        btnStopCapture = new QPushButton(layoutWidget);
        btnStopCapture->setObjectName(QStringLiteral("btnStopCapture"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/img/img/Stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStopCapture->setIcon(icon2);
        btnStopCapture->setIconSize(QSize(24, 24));

        horizontalLayout->addWidget(btnStopCapture);

        hSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(hSpacer_11);

        btnSettings = new QPushButton(layoutWidget);
        btnSettings->setObjectName(QStringLiteral("btnSettings"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/img/img/Gear.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnSettings->setIcon(icon3);
        btnSettings->setIconSize(QSize(24, 24));

        horizontalLayout->addWidget(btnSettings);

        btnAbout = new QPushButton(layoutWidget);
        btnAbout->setObjectName(QStringLiteral("btnAbout"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/img/img/Get Info Blue Button.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnAbout->setIcon(icon4);
        btnAbout->setIconSize(QSize(24, 24));

        horizontalLayout->addWidget(btnAbout);


        verticalLayout->addLayout(horizontalLayout);

        MainWindow->setCentralWidget(cntrlMainWidget);
#ifndef QT_NO_SHORTCUT
        lblCaptureTime->setBuddy(txtCaptureTime);
        lblDestination->setBuddy(txtDestinationPath);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(txtCaptureTime, chkRandomCapture);
        QWidget::setTabOrder(chkRandomCapture, txtDestinationPath);
        QWidget::setTabOrder(txtDestinationPath, btnDestinationBrowser);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "screen-cap", Q_NULLPTR));
        lblCaptureTime->setText(QApplication::translate("MainWindow", "Take a screenshot, once every", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txtCaptureTime->setToolTip(QApplication::translate("MainWindow", "How often to take screenshots?", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        lblTmpSeconds->setText(QApplication::translate("MainWindow", "seconds,", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        chkRandomCapture->setToolTip(QApplication::translate("MainWindow", "Take screenshots at random...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        chkRandomCapture->setText(QApplication::translate("MainWindow", "at random", Q_NULLPTR));
        lblDestination->setText(QApplication::translate("MainWindow", "and then save it in", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        txtDestinationPath->setToolTip(QApplication::translate("MainWindow", "Folder to save the screenshots in...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btnDestinationBrowser->setToolTip(QApplication::translate("MainWindow", "Browse...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnDestinationBrowser->setText(QApplication::translate("MainWindow", "...", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        btnStartCapture->setToolTip(QApplication::translate("MainWindow", "Start Capturing...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnStartCapture->setText(QString());
#ifndef QT_NO_TOOLTIP
        btnStopCapture->setToolTip(QApplication::translate("MainWindow", "Stop Capturing...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnStopCapture->setText(QString());
#ifndef QT_NO_TOOLTIP
        btnSettings->setToolTip(QApplication::translate("MainWindow", "Settings...", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnSettings->setText(QString());
#ifndef QT_NO_TOOLTIP
        btnAbout->setToolTip(QApplication::translate("MainWindow", "Software used by the app..", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnAbout->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
